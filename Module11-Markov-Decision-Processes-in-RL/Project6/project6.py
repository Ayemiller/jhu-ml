import time
from copy import deepcopy

import numpy as np

GAMMA = 0.9  # discount rate
EPSILON = 0.05
VALUE_ITERATION_EPSILON = 0.001
ALPHA = 0.35  # learning rate
MINIMUM_VELOCITY = -5
MAXIMUM_VELOCITY = 5
GOAL = 'F'
WALL = '#'
possible_accelerations = [(1, -1), (1, 1), (1, 0), (0, 1), (0, 0), (-1, -1), (-1, 0), (0, -1), (-1, 1)]


def read_racetrack(filename):
    """ Read in the racetrack from a file.

    Returns:
        List of lists of strings containing the racetrack.
    """
    with open(filename, 'r') as file:
        racetrack_data = [x for x in file.readlines()]
    file.close()
    racetrack = []

    # skip the header line
    for line in racetrack_data[1:]:
        line = line.strip()
        if line == "": continue
        racetrack.append([item for item in line])
    return racetrack


def get_random_starting_coordinates(racetrack):
    """ Return random starting coordinates for the given racetrack

    Args:
        racetrack: the racetrack we're searching

    Returns:
        xy coordinates of a randomly chosen starting spot
    """
    start_list = []

    # iterate over x, then y, to find starting positions
    for x in range(len(racetrack)):
        for y in range(len(racetrack[0])):
            if racetrack[x][y] == 'S':
                start_list += [(x, y)]

    # return a random starting position
    random_index = np.random.choice(len(start_list))
    return start_list[random_index]


def calc_new_bresenham_location(start_x, start_y, x_velocity, y_velocity, racetrack):
    """ Uses Bresenham's line algorithm to generate a sequence of points between two points.

    This sequence is then iterated through to determine the next position.
    If we encounter a wall, we have crashed and we return the point before the wall.
    If we exceed the bound of the racetrack, we call that a crash and return the point before the wall.
    If we encounter the finish line, we're done!
    If we encounter neither, we return the final point.

    Brensenham's line algorithm adapted from slides here: https://inst.eecs.berkeley.edu/~cs150/fa10/Lab/CP3/LineDrawing.pdf

    Args:
        start_x: The starting x coordinate
        start_y: The starting y coordinate
        x_velocity: The x velocity, used to find the next point
        y_velocity: The y velocity, used to find the next point
        racetrack: The racetrack we are using

    Returns:
        The coordinates of the next point using the above rules, and whether or not we have crashed.
    """

    rows = len(racetrack[0])
    cols = len(racetrack)
    interim_point = (start_x, start_y)
    end_x = start_x + x_velocity
    end_y = start_y + y_velocity

    d_err = abs(end_y - start_y)
    y_step = 1
    if start_y > end_y:
        y_step = -1
    x_step = 1
    if start_x > end_x:
        x_step = -1
    d_x = end_x - start_x
    error = d_x / 2
    new_y = start_y
    new_x = start_x
    while new_x != end_x:
        # check if interim point is a goal state, and return it, if so
        if racetrack[new_x][new_y] == GOAL:
            return (new_x, new_y), False
        # check if we hit a wall or exceeded the bounds of the track
        if new_y < 0 or new_y >= rows or new_x < 0 or new_x >= cols:
            return interim_point, True
        error -= d_err
        if error < 0:
            new_y += y_step
            error += d_x
        new_x += x_step
        if racetrack[new_x][new_y] == WALL:
            return interim_point, True
        else:
            interim_point = (new_x, new_y)
    if end_y < 0 or end_y >= rows or end_x < 0 or end_x >= cols or racetrack[end_x][end_y] == WALL:
        return interim_point, True
    else:
        return (end_x, end_y), False


def get_new_velocity(old_vel, accel):
    """ Calculate the new velocity based on previous velocity and provided acceleration.

    Returns:
        object:
    """
    new_x = max(min(old_vel[0] + accel[0], MAXIMUM_VELOCITY), MINIMUM_VELOCITY)
    new_y = max(min(old_vel[1] + accel[1], MAXIMUM_VELOCITY), MINIMUM_VELOCITY)

    # Return the new velocities
    return new_x, new_y


def perform_action(x, y, x_velocity, y_velocity, acceleration, racetrack, crash_back_to_start, deterministic=False):
    """ This method determines the new position and velocity of the car depending on the environment and crash rules.
    The old xy positions and velocity are provided along with the new intended acceleration. The acceleration is
    applied with 80% success, and if the new position does not go through a wall, that new position is returned.

    If it does go through a wall, a crash takes place, and the car is moved to the nearest legal spot, or back to the
    start, if crash_back_to_start is enabled.

    Returns:
        Tuple containing x coord, y coord, x velocity, y velocity
    """

    # acceleration succeeds 80%
    if np.random.uniform() > 0.8 and not deterministic:
        acceleration = (0, 0)

    # calculate new velocity
    new_x_velocity, new_y_velocity = get_new_velocity((x_velocity, y_velocity), acceleration)

    (new_x, new_y), did_crash = calc_new_bresenham_location(x, y, new_x_velocity, new_y_velocity, racetrack)
    if did_crash:
        # go back to start if we are supposed to
        if racetrack[new_x][new_y] != GOAL and crash_back_to_start:
            new_x, new_y = get_random_starting_coordinates(racetrack)
        # velocity after crash is zero
        new_x_velocity, new_y_velocity = 0, 0

    # Return the new state
    return new_x, new_y, new_x_velocity, new_y_velocity, did_crash


def generate_policy_from_q_table(rows, cols, velocity_range, q_table, possible_accelerations):
    """ Helper method to return a dict of the best acceleration policies as prescribed by the Q table.

    Returns:
        Policy dict that contains the best acceleration for each combination of x, y, x-velocity and y-velocity.
    """
    policy_dict = {}
    # find the best acceleration action for each position and velocity
    for y in range(rows):
        for x in range(cols):
            for y_velocity in velocity_range:
                for x_velocity in velocity_range:
                    policy_dict[(x, y, x_velocity, y_velocity)] = possible_accelerations[
                        np.argmax(q_table[x][y][x_velocity][y_velocity])]

    return policy_dict


def q_learning(q_table, x, y, x_velocity, y_velocity, acceleration, new_x, new_y, new_x_velocity, new_y_velocity,
               reward):
    """ The update rule for q-learning.
    Uses the algorithm specified in the textbook by finding the best acceleration in the current policy of the new point and velocity to update the q-value

    Args:
        q_table: the existing q table
        x: the current x coordinate
        y: the current y coordinate
        x_velocity: the current x velocity
        y_velocity: the current y velocity
        acceleration: the current acceleration
        new_x: the new x coordinate
        new_y: the new y coordinate
        new_x_velocity: the new x velocity
        new_y_velocity: the new y velocity
        reward: the specified reward
    """
    q_table[x][y][x_velocity][y_velocity][acceleration] = (
            (1 - ALPHA) * q_table[x][y][x_velocity][y_velocity][acceleration] + ALPHA * (
            reward + GAMMA * max(q_table[new_x][new_y][new_x_velocity][new_y_velocity])))


def sarsa(q_table, x, y, x_velocity, y_velocity, acceleration, new_x, new_y, new_x_velocity, new_y_velocity, reward):
    """  The update rule for SARSA
    Uses the update specified in the textbook by first finding the the action of the new point and using that to update
    the Q value of the new point.

    Args:
        q_table: the existing q table
        x: the current x coordinate
        y: the current y coordinate
        x_velocity: the current x velocity
        y_velocity: the current y velocity
        acceleration: the current acceleration
        new_x: the new x coordinate
        new_y: the new y coordinate
        new_x_velocity: the new x velocity
        new_y_velocity: the new y velocity
        reward: the specified reward
    """
    new_acceleration = np.argmax(q_table[new_x][new_y][new_x_velocity][new_y_velocity])
    q_table[x][y][x_velocity][y_velocity][acceleration] = (
            (1 - ALPHA) * q_table[x][y][x_velocity][y_velocity][acceleration] + ALPHA * (
            reward + GAMMA * q_table[new_x][new_y][new_x_velocity][new_y_velocity][new_acceleration]))


def train_value_iteration(racetrack, crash_back_to_start, number_of_training_iterations, show_track_updates,
                          time_trial_after_iteration):
    """ Helper method to train and test the tracks using the value iteration algorithm

    Args:
        racetrack: the racetrack we're using
        crash_back_to_start: whether to return back to the start when we crash
        number_of_training_iterations: the max number of training iterations
        show_track_updates: whether or not to display the first time trial
        time_trial_after_iteration: the iteration after which we will start to perform time trials

    """
    time_trial_action_cutoff = 500
    velocity_range = range(MINIMUM_VELOCITY, MAXIMUM_VELOCITY + 1)
    rows = len(racetrack[0])
    cols = len(racetrack)

    # initialize the v-table and the q-table to 0
    v_table = [[[[0 for x_velocity in velocity_range] for y_velocity in velocity_range] for character in row] for row in
               racetrack]
    q_table = [[[[[0 for acceleration in possible_accelerations] for x_velocity in velocity_range] for y_velocity in (
            velocity_range)] for character in row] for row in racetrack]
    start_time = time.time()
    # main value iteration training loop
    for training_iteration in range(number_of_training_iterations):

        # copy v-table so we can calculate V(t-1)
        old_v_table = deepcopy(v_table)

        # iterate through all the possible states
        for x in range(cols):
            for y in range(rows):
                for x_velocity in velocity_range:
                    for y_velocity in velocity_range:

                        # set a negative reward for walls so we try to avoid them
                        if racetrack[x][y] == WALL:
                            v_table[x][y][x_velocity][y_velocity] = -2
                        else:
                            # For each action a in the set of possible actions A
                            for ai, a in enumerate(possible_accelerations):

                                # The reward is -1 for every state except
                                # for the finish line states
                                if racetrack[x][y] == GOAL:
                                    r = 100
                                else:
                                    r = -1

                                # Get the new state s'. s' is based on the current
                                # state s and the current action a
                                new_x, new_y, new_vx, new_vy, _ = perform_action(
                                    x, y, x_velocity, y_velocity, a, racetrack, crash_back_to_start, True)

                                # V(s'): value of the new state when taking action
                                # a from state s. This is the one step look ahead.
                                value_of_new_state = old_v_table[new_x][new_y][
                                    new_vx][new_vy]

                                # Get the new state s'. s' is based on the current
                                # state s and the action (0,0)
                                new_x, new_y, new_vx, new_vy, _ = perform_action(
                                    x, y, x_velocity, y_velocity, (0, 0), racetrack, crash_back_to_start, True)

                                # V(s'): value of the new state when taking action
                                # (0,0) from state s. This is the value if for some
                                # reason the race car attemps to accelerate but
                                # fails
                                value_of_new_state_if_action_fails = old_v_table[
                                    new_x][new_y][new_vx][new_vy]

                                # Expected value of the new state s'
                                # Note that each state-action pair has a unique
                                # value for s'
                                expected_value = 0.8 * value_of_new_state + 0.2 * value_of_new_state_if_action_fails

                                # Update the Q-value in Q[s,a]
                                # immediate reward + discounted future value
                                q_table[x][y][x_velocity][y_velocity][ai] = r + (GAMMA * expected_value)

                        # Get the action with the highest Q value
                        argMaxQ = np.argmax(q_table[x][y][x_velocity][y_velocity])

                        # Update V(s)
                        v_table[x][y][x_velocity][y_velocity] = q_table[x][y][x_velocity][y_velocity][argMaxQ]

        # reset all goals positions on the track to 0 so they don't throw off the stopping calculation
        for x in range(cols):
            for y in range(rows):
                # Terminal state has a value of 0
                if racetrack[x][y] == GOAL:
                    for x_velocity in velocity_range:
                        for y_velocity in velocity_range:
                            v_table[x][y][x_velocity][y_velocity] = 0

        # calculate max |Vt(s) - Vt-1(s)| to determine if the values have converged
        v_table_difference = max([max([max([max([abs(v_table[x][y][x_velocity][y_velocity] - old_v_table[x][
            y][x_velocity][y_velocity]) for x_velocity in velocity_range]) for y_velocity in (
                                                    velocity_range)]) for x in range(cols)]) for y in range(rows)])
        print(f'Iteration {training_iteration}:\tmax |Vt(s) - Vt-1(s)| = {v_table_difference}')

        # start performing time-trials after 16 iterations, when things begin to settle down
        if training_iteration > time_trial_after_iteration:
            policy = (generate_policy_from_q_table(rows, cols, velocity_range, q_table, possible_accelerations))
            print(f' --- Duration: {round(time.time() - start_time, 2)} seconds ---')
            avg_number_of_actions_in_timetrial = perform_time_trial(racetrack, policy, crash_back_to_start,
                                                                    show_track_updates,
                                                                    time_trial_action_cutoff)
            print(f'Average number of actions in time trial {avg_number_of_actions_in_timetrial}')

        # exit the training loop if our difference value falls below epsilon
        if v_table_difference < VALUE_ITERATION_EPSILON:
            return


def train_q_learning(racetrack, crash_back_to_start, number_of_training_iterations, update_method):
    """ Method to train a given racetrack using a given update method, either sarsa or q-learning.

    The method performs a specified number of training iterations and up to 75 "moves" are permitted per episode if
    a goal is not reached.

    Each episode begins by choosing a random starting point (that is not a wall or the finish) and a random velocity.

    An epsilon greedy approach to acceleration selection is used. In general, for (1-epsilon)% of the time, a greedy
    the best known acceleration action is taken, otherwise a random acceleration is selected. However, in the first
    quarter of training iterations, we double the epsilon value to encourage exploration. In the final quarter of
    training iterations, we divide the epsilon value by a factor of 5 to "cool it down" and encourage exploitation.

    We assign a general reward of -1 to normal actions, -2 to a crash, and 100 for making it to the finish line.

    Args:
        racetrack:
        crash_back_to_start:
        number_of_training_iterations:
        update_method: the method to update the q-table, either q-learning or SARSA
    Returns:

    """
    rows = len(racetrack[0])
    cols = len(racetrack)
    velocity_range = range(MINIMUM_VELOCITY, MAXIMUM_VELOCITY + 1)

    # initialize all of the q values to zero
    q_table = [[[[[0 for acceleration in possible_accelerations] for x_velocity in velocity_range] for y_velocity in
                 velocity_range] for character in row] for row in racetrack]

    # main training loop
    for training_iteration in range(number_of_training_iterations):

        # choose a random starting point
        x = np.random.choice(range(cols))
        y = np.random.choice(range(rows))

        # don't let us randomly start on wall or the goal
        while racetrack[x][y] == GOAL or racetrack[x][y] == WALL:
            x = np.random.choice(range(cols))
            y = np.random.choice(range(rows))
        x_velocity = np.random.choice(velocity_range)
        y_velocity = np.random.choice(velocity_range)

        # perform some crude "annealing" of Epsilon as a function of how many iterations have been performed
        annealed_epsilon = EPSILON
        if training_iteration < number_of_training_iterations / 4:
            # in the first quarter of iterations, heat up epsilon by a factor of 2
            annealed_epsilon *= 2
        elif training_iteration > number_of_training_iterations / 4 * 3:
            # in the final quarter of iterations, cool down epsilon by a factor of 5
            annealed_epsilon /= 5

        current_training_iterations = 0
        while racetrack[x][y] != GOAL and current_training_iterations < 75:
            # for (1-epsilon)% of the time, choose the best action for the state s, otherwise, choose randomly
            if np.random.uniform() >= annealed_epsilon:
                acceleration = np.argmax(q_table[x][y][x_velocity][y_velocity])
            else:
                acceleration = np.random.choice(len(possible_accelerations))

            # Act and then observe a new state s'
            new_x, new_y, new_x_velocity, new_y_velocity, did_crash = perform_action(x, y, x_velocity, y_velocity,
                                                                                     possible_accelerations[
                                                                                         acceleration],
                                                                                     racetrack,
                                                                                     crash_back_to_start=crash_back_to_start)
            if racetrack[new_x][new_y] == GOAL:
                reward = 100
            elif did_crash:
                reward = -2
            else:
                reward = -1
            # update our q-table using the provided update method argument, either q-learning or SARSA
            # immediate reward received from taking action a in state s plus the discounted future reward
            update_method(q_table, x, y, x_velocity, y_velocity, acceleration, new_x, new_y, new_x_velocity,
                          new_y_velocity,
                          reward)

            # update our current state
            x, y, x_velocity, y_velocity = new_x, new_y, new_x_velocity, new_y_velocity
            current_training_iterations += 1
    return generate_policy_from_q_table(rows, cols, velocity_range, q_table, possible_accelerations)


def pretty_print_racetrack(racetrack, car_position):
    """ Print the racetrack displaying the car's current position as 'X'.

    Args:
        racetrack: the racetrack that we're using
        car_position: the x,y coordinates of the car on the racetrack
    """
    racetrack = deepcopy(racetrack)
    racetrack = np.transpose(racetrack).tolist()
    # display the car on the racetrack as 'X'
    racetrack[car_position[1]][car_position[0]] = 'X'

    for row in racetrack:
        for character in row:
            print(character, end="")
        print()
    print()


def perform_time_trial(racetrack, policy, crash_back_to_start, show_track_updates, num_actions_cutoff):
    """ Perform 20 time trials using the provided policy and return the number of actions it takes to reach the finish.

    Returns:
        The average number of steps that it took to perform the 20 time trials.
    """
    total_steps = 0
    number_of_time_trials = 20
    for time_trial_index in range(number_of_time_trials):

        # put stopped racer on the starting block
        x, y = get_random_starting_coordinates(racetrack)
        x_velocity, y_velocity = 0, 0

        # for i in range(num_actions_cutoff):
        time_trial_step_index = 0
        while time_trial_step_index < num_actions_cutoff and racetrack[x][y] != GOAL:
            # display the racetrack updates for each move
            if show_track_updates and time_trial_index < 1:
                pretty_print_racetrack(racetrack, (x, y))

            # choose the acceleration action from the policy
            acceleration = policy[(x, y, x_velocity, y_velocity)]

            # get a new position and velocity by performing an action
            x, y, x_velocity, y_velocity, _ = perform_action(x, y, x_velocity, y_velocity, acceleration, racetrack,
                                                             crash_back_to_start)
            time_trial_step_index += 1
        if verbose:
            print(f'Number of actions in iteration {time_trial_index}: {time_trial_step_index}')
        total_steps += time_trial_step_index

    # return the average number of actions
    return total_steps / number_of_time_trials


def train_test_plot(racetrack_name, number_of_iterations, crash_back_to_start, show_track_updates, update_method):
    """ Reads in the race track and performs either SARSA or q-learning, as specified.

    Args:
        racetrack: the racetrack we're using
        number_of_iterations: the number of training iterations
        crash_back_to_start: whether to return back to the start when we crash
        show_track_updates: whether or not to display the first time trial
        update_method: method to use for updates, either SARSA or Q-learning
    """
    print(f'\nTraining {racetrack_name} using {update_method.__name__} for {number_of_iterations} iterations.')
    print(f'Crash Back to Start: {str(crash_back_to_start)}')
    print(f'Alpha={ALPHA}, Gamma={GAMMA}, Epsilon={EPSILON}')
    racetrack = np.transpose(read_racetrack(racetrack_name)).tolist()

    # Keep track of the total number of steps
    total_steps = 0
    time_trial_action_cutoff = 500
    start_time = time.time()
    # Retrieve the policy
    policy = train_q_learning(racetrack, crash_back_to_start, number_of_iterations, update_method)
    print(f' --- Duration: {round(time.time() - start_time, 2)} seconds ---')

    avg_number_of_actions_in_timetrial = perform_time_trial(racetrack, policy, crash_back_to_start, show_track_updates,
                                                            time_trial_action_cutoff)
    print(f'Average number of actions in time trial {avg_number_of_actions_in_timetrial}')


def train_test_plot_value_iteration(racetrack_name, number_of_iterations, crash_back_to_start, show_track_updates,
                                    time_trial_after_iteration):
    """ Reads in the racetrack and calls the value iteration algorithm

    Args:
        racetrack: the racetrack we're using
        crash_back_to_start: whether to return back to the start when we crash
        number_of_training_iterations: the max number of training iterations
        show_track_updates: whether or not to display the first time trial
        time_trial_after_iteration: the iteration after which we will start to perform time trials

    """
    print(f'\nTraining {racetrack_name} using value iteration for {number_of_iterations} iterations.')
    print(f'Crash Back to Start: {str(crash_back_to_start)}')
    print(f'Gamma={GAMMA}, Epsilon={VALUE_ITERATION_EPSILON}')
    racetrack = np.transpose(read_racetrack(racetrack_name)).tolist()

    train_value_iteration(racetrack, crash_back_to_start, number_of_iterations, show_track_updates,
                          time_trial_after_iteration)


track = 'R-track.txt'
verbose = False
crash_back_to_start = False
show_track_updates = False

train_test_plot_value_iteration(track, 40, crash_back_to_start, show_track_updates, time_trial_after_iteration=17)
crash_back_to_start = True
train_test_plot_value_iteration(track, 40, crash_back_to_start, show_track_updates, time_trial_after_iteration=17)
show_track_updates = True
crash_back_to_start = False
train_test_plot(track, 1_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 10_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 40_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 60_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 80_000, crash_back_to_start, show_track_updates, q_learning)
# train_test_plot(track, 100_000, crash_back_to_start, show_track_updates, q_learning)
# train_test_plot(track, 150_000, crash_back_to_start, show_track_updates, q_learning)
# train_test_plot(track, 200_000, crash_back_to_start, show_track_updates, q_learning)
# train_test_plot(track, 400_000, crash_back_to_start, show_track_updates, q_learning)
# train_test_plot(track, 800_000, crash_back_to_start, show_track_updates, q_learning)
# train_test_plot(track, 1_500_000, crash_back_to_start, show_track_updates, q_learning)

show_track_updates = True
crash_back_to_start = True
train_test_plot(track, 1_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 10_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 40_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 60_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 80_000, crash_back_to_start, show_track_updates, q_learning)
# train_test_plot(track, 100_000, crash_back_to_start, show_track_updates, q_learning)
# train_test_plot(track, 150_000, crash_back_to_start, show_track_updates, q_learning)
# train_test_plot(track, 200_000, crash_back_to_start, show_track_updates, q_learning)
# train_test_plot(track, 400_000, crash_back_to_start, show_track_updates, q_learning)
# train_test_plot(track, 800_000, crash_back_to_start, show_track_updates, q_learning)
# train_test_plot(track, 1_500_000, crash_back_to_start, show_track_updates, q_learning)

crash_back_to_start = False
show_track_updates = False
train_test_plot(track, 1_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 10_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 40_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 60_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 80_000, crash_back_to_start, show_track_updates, sarsa)
# train_test_plot(track, 100_000, crash_back_to_start, show_track_updates, sarsa)
# train_test_plot(track, 150_000, crash_back_to_start, show_track_updates, sarsa)
# train_test_plot(track, 200_000, crash_back_to_start, show_track_updates, sarsa)
# train_test_plot(track, 400_000, crash_back_to_start, show_track_updates, sarsa)
# train_test_plot(track, 800_000, crash_back_to_start, show_track_updates, sarsa)
# train_test_plot(track, 1_500_000, crash_back_to_start, show_track_updates, sarsa)
# train_test_plot(track, 3_000_000, crash_back_to_start, show_track_updates, sarsa)

crash_back_to_start = True
show_track_updates = False
train_test_plot(track, 1_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 10_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 40_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 60_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 80_000, crash_back_to_start, show_track_updates, sarsa)

track = 'O-track.txt'
crash_back_to_start = False
show_track_updates = False

train_test_plot_value_iteration(track, 40, crash_back_to_start, show_track_updates, time_trial_after_iteration=0)

train_test_plot(track, 1_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 10_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 40_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 60_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 80_000, crash_back_to_start, show_track_updates, q_learning)

crash_back_to_start = False
show_track_updates = False
train_test_plot(track, 1_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 10_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 40_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 60_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 80_000, crash_back_to_start, show_track_updates, sarsa)

track = 'L-track.txt'
crash_back_to_start = False
show_track_updates = False

train_test_plot_value_iteration(track, 40, crash_back_to_start, show_track_updates, time_trial_after_iteration=8)

train_test_plot(track, 1_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 10_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 40_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 60_000, crash_back_to_start, show_track_updates, q_learning)
train_test_plot(track, 80_000, crash_back_to_start, show_track_updates, q_learning)

crash_back_to_start = False
show_track_updates = False
train_test_plot(track, 1_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 10_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 40_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 60_000, crash_back_to_start, show_track_updates, sarsa)
train_test_plot(track, 80_000, crash_back_to_start, show_track_updates, sarsa)
