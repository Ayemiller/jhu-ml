import math
import random
###
# amill189 aka Adam Miller
###
import sys
import time
from collections import defaultdict
from typing import Dict, List, Tuple

cardinal_moves = [(0, -1), (1, 0), (0, 1), (-1, 0)]
costs = {'.': -1, '*': -3, '#': -5, '~': -7}


def get_node_cost_from_world(node: Tuple[int, int], world: List[List[str]]) -> int:
    """ Helper function to return the cost of a node in the world.

    Args:
        node: A given node in the world.
        world: The world we are searching.

    Returns:
        Cost of a given node in the world.

    """
    return costs[world[node[1]][node[0]]]


def valid(next_action: Tuple[int, int], world: List[List[str]]) -> bool:
    """ Function that returns whether or not a given x,y coordinate Tuple is valid

    Args:
        next_action: a Tuple representing an x,y coordinate
        world: the world we are searching

    Returns: true if valid, false otherwise

    """
    dimension = len(world[0])
    return 0 <= next_action[0] < dimension and 0 <= next_action[1] < dimension and \
           world[next_action[1]][next_action[0]] != 'x'


def q_learning(world: List[List[str]], costs: Dict[str, int], goal: Tuple[int, int], reward: int,
               actions: List[Tuple[int, int]], gamma: float, alpha: float):
    """ Q-learning function to generate a policy of moves.

    This q-learning function is based off of the pseudo-code found in the
    Barto and Sutton along with the pseudo-code provided in module 4.

    The function creates a Q action value dictionary that initializes
    all unknown keys to the value 100. It performs 100,000 "episodes" as
    that number seemed to provide an adequate amount coverage to both the
    small and large worlds.

    The function starts exploring from a random spot on the map (provided
    that it is not the goal and it's not in the mountains).

    For each episode, the function loops until the goal is reached.

    First, it finds the probabilities for the next state, and it chooses
    one based on the weights determined by 'get_probabilities_for_next_state'.

    The reward is then calculated, either as a gigantic goal reward, or as
    a cost incurred by whatever terrain was chosen as the next_state.

    Then the Q value for the current state is amended, according to the
    equation provided: Q[s, a] := (1-α)Q[s, a] + α(r + ɣ max Q[s',a'])

    If the goal was chosen as the next state, the episode ends, otherwise,
    it continues looping until we find the goal state.

    Finally, after all the episodes have been completed, the Q dict is
    converted to a policy that dictates the "correct" move to make from
    every valid point in the world.

    Args:
        world: the world we are searching
        costs: a dict that defines the various "costs" associated with the
            different types of terrain in our world
        goal: a tuple representing the x,y coordinates of our goal node
        reward: the reward for achieving the goal state
        actions: a list of possible actions as offset tuples
        gamma: the discount rate
        alpha: the learning rate
    Returns:
        Dict containing the policy which has keys of x,y Tuples and the
        action to perform, as an x,y offset Tuple.
        
    """

    q: Dict[Tuple[Tuple[int, int], Tuple[int, int]], float] = defaultdict(lambda: float(100))
    for i in range(100_000):

        # choose a random starting point
        state = (random.randint(0, len(world[0]) - 1), random.randint(0, len(world[0]) - 1))

        # don't let us randomly start on mountains or the goal
        while world[state[1]][state[0]] == 'x' or state == goal:
            state = (random.randint(0, len(world[0]) - 1), random.randint(0, len(world[0]) - 1))

        # loop until you find the goal
        while True:

            # figure out our successor state
            probabilities = get_probabilities_for_next_state(state, q, world)
            action = actions[random.choices(range(4), probabilities)[0]]
            next_state: Tuple[int, int] = (state[0] + action[0], state[1] + action[1])

            # set the appropriate reward, either the cost of the move or the goal bonus
            if next_state != goal:
                episode_reward = get_node_cost_from_world(next_state, world)
            else:
                episode_reward = reward

            next_action = actions[
                argmax(get_valid_moves_from_state(next_state, world), q, next_state)]

            q[(state, action)] += alpha * (
                    (episode_reward + gamma * q[(next_state, next_action)]) - q[(state, action)])

            # we reached the goal, end the episode
            if next_state == goal:
                break

            # advance our state
            state = next_state

    # convert our Q dict to a policy of moves
    return convert_action_utility_to_policy(q, world, goal)


def argmax(args: Dict[Tuple[int, int], Tuple[int, int]], q: Dict[Tuple[Tuple[int, int], Tuple[int, int]], float],
           state: Tuple[int, int]) -> int:
    """ Argmax function to return the arguments of the move that maximizes value.

    This function determines the move that would provide the maximum value according
    to the Q action-value dict.

    If one or more moves provide an equal value, this function will randomly choose
    a value to apply, to promote exploration.

    Args:
        args: Dict containing a move mapped to the state it would move to.
        q: Action-value dict
        state: The current state

    Returns:
        The index of the move to make in order to maximize value according to the action-value dict.

    """
    max_args = args
    max_value = -sys.maxsize
    for k, v in args.items():
        value = q[state, k]
        if value > max_value:
            # this move gives the maximum value
            max_value = value
            max_args = k
        elif math.isclose(value, max_value):
            # randomly swap when "equal"
            if random.randint(0, 1) == 1:
                max_value = value
                max_args = k
    # return the move
    return cardinal_moves.index(max_args)


def get_state(state: Tuple[int, int], move: Tuple[int, int]) -> Tuple[int, int]:
    """ Helper function to return an x,y Tuple of the state after a move is applied.

    Args:
        state: the current state
        move: the move to be applied to the current state

    Returns:
        The x,y Tuple containing the current state with the specified move applied.

    """
    return state[0] + move[0], state[1] + move[1]


def get_probabilities_for_next_state(state: Tuple[int, int], q: Dict[Tuple[Tuple[int, int], Tuple[int, int]], float],
                                     world: List[List[str]]) -> List[float]:
    """ Helper function to create a list of probabilities that weight the next move.

    The "best" move to make is given a weight of 70%. The remaining 30% is split
    evenly amongst the remaining valid moves. That is, if there are 3 other moves,
    they each get 10%. If there are are 2 other moves, they each get 15%, and so on.

    Args:
        state: The current state.
        q: The action-value dict.
        world: The world we are searching.

    Returns:
        A list containing the probabilities that are assigned to each potential move.

    """
    valid_moves = get_valid_moves_from_state(state, world)
    best_move = argmax(valid_moves, q, state)

    # initialize the four potential move options to 0
    probabilities = [0] * 4

    # set the valid moves to the minimum non-zero value
    if len(valid_moves) > 1:
        for k in valid_moves.keys():
            probabilities[cardinal_moves.index(k)] = 0.3 / (len(valid_moves) - 1)

    # set the best move to 70%
    probabilities[best_move] = 0.7

    # probabilities of next move in order down, right, up, left
    return probabilities


def get_valid_moves_from_state(state: Tuple[int, int], world: List[List[str]]) -> Dict[
    Tuple[int, int], Tuple[int, int]]:
    """ Helper function to provide a dict of valid moves from a given state.

    Args:
        state: The provided state.
        world: The world we are searching.

    Returns:
        A dict containing the valid moves from the given state and the new x,y Tuple that
        they map to.

    """
    valid_moves = {}
    for move in cardinal_moves:
        next_move = get_state(state, move)
        if valid(next_move, world):
            valid_moves[move] = get_state(state, next_move)
    return valid_moves


def convert_action_utility_to_policy(q: Dict[Tuple[Tuple[int, int], Tuple[int, int]], float], world: List[List[str]],
                                     goal: Tuple[int, int]) -> Dict[Tuple[int, int], Tuple[int, int]]:
    """ Helper function that iterates through all of the information in our "Q" action-value dict and returns a policy.

    This functions looks at every x,y coordinate in our world and assigns a "correct" move based on
    the values in the Q dict.
    
    If there are mountains in the world, the policy assigns it a move of (1,1) which will tell the
    print method to print a mountain.
    
    The goal state will be assigned a move of (2,2) to tell the print method to print a 'G' to
    denote the goal state.
    
    Args:
        q: The action-value Q dict.
        world: The world we are searching.
        goal: The goal state.

    Returns:
        A policy that contains the ideal move from every coordinate in the world.
        
    """
    policy = {}
    # iterate over x, then y
    for x in range(len(world[0])):
        for y in range(len(world[0])):
            if world[y][x] == 'x':
                policy[(x, y)] = (1, 1)
            elif (x, y) == goal:
                policy[(x, y)] = (2, 2)
            else:
                # create a temporary dict containing the possible moves
                possible_moves = {move: move for move in cardinal_moves}

                # assign the move which has the highest value in the Q dict
                policy[(x, y)] = cardinal_moves[argmax(possible_moves, q, (x, y))]

    return policy


def pretty_print_policy(cols: int, rows: int, policy: Dict[Tuple[int, int], Tuple[int, int]]):
    """ Helper function to print the results from our policy.

    This function has a dict that maps values in the policy to the proper character to be printed.
    The various moves in the policy map to the appropriate arrows. The value (1,1) is a flag to
    print mountains and the value (2,2) is a flag to print the goal state.

    Args:
        cols: The number of columns.
        rows: The number of rows.
        policy: The policy generated by our Q-learning method.

    """
    arrows: Dict[Tuple[int, int], str] = {(0, -1): '^', (1, 0): '>', (0, 1): 'V', (-1, 0): '<', (1, 1): 'x',
                                          (2, 2): 'G'}
    for y in range(rows):
        for x in range(cols):
            print(arrows[policy[(x, y)]], end="")
        print("")
    print("")


def read_world(filename):
    with open(filename, 'r') as f:
        world_data = [x for x in f.readlines()]
    f.closed
    world = []
    for line in world_data:
        line = line.strip()
        if line == "": continue
        world.append([x for x in line])
    return world


if __name__ == "__main__":
    # use this debug flag if necessary; otherwise, the program should print out only
    # what is requested.
    debug = len(sys.argv) > 1 and sys.argv[1].lower() == 'debug'
    # Q-Learning!
    goal = (5, 5)
    reward = 10_000
    gamma = 0.95  # discount rate
    alpha = 0.6  # learning rate
    start_time = time.time()
    world = read_world("small.txt")
    full_policy = q_learning(world, costs, goal, reward, cardinal_moves, gamma, alpha)
    pretty_print_policy(len(world[0]), len(world[0]), full_policy)
    if debug:
        print(f'\n\nProgram Execution Duration')
        print(f' --- {round(time.time() - start_time, 2)} seconds ---')

    start_time = time.time()
    goal = (26, 26)
    reward = 10_000_000
    gamma = 0.95  # discount rate
    alpha = 0.6  # learning rate

    world = read_world("large.txt")
    full_policy = q_learning(world, costs, goal, reward, cardinal_moves, gamma, alpha)
    pretty_print_policy(len(world[0]), len(world[0]), full_policy)
    if debug:
        print(f'\n\nProgram Execution Duration')
        print(f' --- {round(time.time() - start_time, 2)} seconds ---')
