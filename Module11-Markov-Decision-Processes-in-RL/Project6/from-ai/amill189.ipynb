{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Module 4 - Programming Assignment\n",
    "\n",
    "## Directions\n",
    "\n",
    "There are general instructions on Blackboard and in the Syllabus for Programming Assignments. This Notebook also has instructions specific to this assignment. Read all the instructions carefully and make sure you understand them. Please ask questions on the discussion boards or email me at `EN605.445@gmail.com` if you do not understand something.\n",
    "\n",
    "<div style=\"background: mistyrose; color: firebrick; border: 2px solid darkred; padding: 5px; margin: 10px;\">\n",
    "Please follow the directions and make sure you provide the requested output. Failure to do so may result in a lower grade even if the code is correct or even 0 points.\n",
    "</div>\n",
    "\n",
    "You must submit your assignment as `<jhed_id>.ipynb` but DO NOT submit the supplementary files. Thanks!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "import math\n",
    "import random\n",
    "import sys\n",
    "import time\n",
    "from collections import defaultdict\n",
    "from typing import Dict, List, Tuple, Any\n",
    "from IPython.core.display import *\n",
    "# add whatever else you need from the Anaconda packages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reinforcement Learning with Q-Learning\n",
    "\n",
    "The world for this problem is very similar to the one from Module 1 that we solved with A\\* search but this time we're going to use a different approach.\n",
    "\n",
    "We're replacing the deterministic world with stochastic one. This means, when the agent moves \"south\" instead of always going \"south\", there is a probability distribution of possible successor states \"south\", \"east\", \"north\" and \"west\". This is meant to suggest things like wheels slipping or other factors that may lead an agent to be other than in the intended spot. So the agent may not end up in the state it wanted.\n",
    "\n",
    "There are a variety of ways to handle this problem. For example, if using A\\* search, if the agent finds itself off the solution, you can simply calculate a new solution from where the agent ended up. Although this sounds like a really bad idea, it has actually been shown to work really well in Video Games that use formal Planning algorithms (which we will cover later). When these algorithms were first designed, this was unthinkable. Thank you, Moore's Law!\n",
    "\n",
    "Another approach is to use Reinforcement Learning which covers problems where there is some kind of general uncertainty. As it turns out, Reinforcement Learning is actually really good for the kinds of controls problems that we can't completely specify (and that inability to specify everything exactly is where the ability to plan under uncertainty comes in handy...flying a helicopter, flipping a pancake, kicking a soccer ball). \n",
    "\n",
    "In this assignment, we're going to model that uncertainty a bit unrealistically but it'll show you how the algorithm works.\n",
    "\n",
    "As far as RL is concerned, there are a variety of options there: model-based and model-free, Value Iteration, Q-Learning and SARSA. You are going to use Value Iteration and Q-Learning."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The World Representation\n",
    "\n",
    "As before, we're going to simplify the problem by working in a grid world. The symbols that form the grid have a special meaning as they specify the type of the terrain and the cost to enter a grid cell with that type of terrain:\n",
    "\n",
    "```\n",
    "token   terrain    cost \n",
    ".       plains     1\n",
    "*       forest     3\n",
    "#       hills      5\n",
    "~       swamp      7\n",
    "x       mountains  impassible\n",
    "```\n",
    "\n",
    "When you go from a plains node to a forest node it costs 3. When you go from a forest node to a plains node, it costs 1. You can think of the grid as a big graph. Each grid cell (terrain symbol) is a node and there are edges to the north, south, east and west (except at the edges).\n",
    "\n",
    "There are quite a few differences between A\\* Search and Reinforcement Learning but one of the most salient is that A\\* Search returns a plan of N steps that gets us from A to Z, for example, A->C->E->G.... Reinforcement Learning, on the other hand, returns  a *policy* that tells us the best thing to do **for every and any state.**\n",
    "\n",
    "For example, the policy might say that the best thing to do in A is go to C. However, we might find ourselves in D instead. But the policy covers this possibility, it might say, D->E. Trying this action might land us in C and the policy will say, C->E, etc. At least with offline learning, everything will be learned in advance (in online learning, you can only learn by doing and so you may act according to a known but suboptimal policy).\n",
    "\n",
    "Nevertheless, if you were asked for a \"best case\" plan from (0, 0) to (n-1, n-1), you could (and will) be able to read it off the policy because there is a best action for every state. You will be asked to provide this in your assignment."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading the Map\n",
    "\n",
    "To avoid global variables, we have a <code>read_world()</code> function that takes a filename and returns the world as `List` of `List`s. **The same coordinates reversal applies: (x, y) is world[ y][ x] as from PR01.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "def read_world( filename):\n",
    "    with open( filename, 'r') as f:\n",
    "        world_data = [x for x in f.readlines()]\n",
    "    f.closed\n",
    "    world = []\n",
    "    for line in world_data:\n",
    "        line = line.strip()\n",
    "        if line == \"\": continue\n",
    "        world.append([x for x in line])\n",
    "    return world"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we create a dict of movement costs. Note that we've negated them this time because RL requires negative costs and positive rewards:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "COSTS = { '.': -1, '*': -3, '#': -5, '~': -7}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and a list of offsets for `cardinal_moves`. You'll need to work this into your actions, A, parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "CARDINAL_MOVES = [(0,-1), (1,0), (0,1), (-1,0)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now the confusing bits begin. We must program both the Q-Learning algorithm and a *simulator*. The Q-Learning algorithm doesn't know T but the simulator *must*. Essentially the *simulator* is any time you apply a move and check to see what state you actually end up in (rather than the state you planned to end up in).\n",
    "\n",
    "The transition function your *simulation* should use, T, is 0.70 for the desired direction, and 0.10 each for the other possible directions. That is, if I select \"up\" then 70% of the time, I go up but 10% of the time I go left, 10% of the time I go right and 10% of the time I go down. If you're at the edge of the map, you simply bounce back to the current state.\n",
    "\n",
    "You need to implement `q_learning()` with the following parameters:\n",
    "\n",
    "+ world: a `List` of `List`s of terrain (this is S from S, A, T, gamma, R)\n",
    "+ costs: a `Dict` of costs by terrain (this is part of R)\n",
    "+ goal: A `Tuple` of (x, y) stating the goal state.\n",
    "+ reward: The reward for achieving the goal state.\n",
    "+ actions: a `List` of possible actions, A, as offsets.\n",
    "+ gamma: the discount rate\n",
    "+ alpha: the learning rate\n",
    "\n",
    "you will return a policy: \n",
    "\n",
    "`{(x1, y1): action1, (x2, y2): action2, ...}`\n",
    "\n",
    "Remember...a policy is what to do in any state for all the states. Notice how this is different that A\\* search which only returns actions to take from the start to the goal. This also explains why `q_learning` doesn't take a `start` state!\n",
    "\n",
    "You should also define a function `pretty_print_policy( cols, rows, policy)` that takes a policy and prints it out as a grid using \"^\" for up, \"<\" for left, \"v\" for down and \">\" for right. Note that it doesn't need the `world` because the policy has a move for every state. However, you do need to know how big the grid is so you can pull the values out of the `Dict` that is returned.\n",
    "\n",
    "```\n",
    "vvvvvvv\n",
    "vvvvvvv\n",
    "vvvvvvv\n",
    ">>>>>>v\n",
    "^^^>>>v\n",
    "^^^>>>v\n",
    "^^^>>>G\n",
    "```\n",
    "\n",
    "(Note that that policy is completely made up and only illustrative of the desired output).\n",
    "\n",
    "There are a lot of details that I have left up to you. For example, when do you stop? Is there a strategy for learning the policy? Watch and re-watch the lecture on Q-Learning. Ask questions. You need to implement a way to pick initial states for each iteration and you need a way to balance exploration and exploitation while learning. You may have to experiment with different gamma and alpha values. Be careful with your reward...the best reward is related to the discount rate and the approxmiate number of actions you need to reach the goal.\n",
    "\n",
    "* If everything is otherwise the same, do you think that the path from (0,0) to the goal would be the same for both A\\* Search and Q-Learning?\n",
    "* What do you think if you have a map that looks like:\n",
    "\n",
    "```\n",
    "><>>^\n",
    ">>>>v\n",
    ">>>>v\n",
    ">>>>v\n",
    ">>>>G\n",
    "```\n",
    "\n",
    "has this converged? Is this a \"correct\" policy?\n",
    "\n",
    "**I strongly suggest that you implement Value Iteration on your own to solve these problems.** Why? Because Value Iteration will find the policy to which Q-Learning should coverge in the limit. If you include your Value Iteration implementation and output, I will count it towards your submission grade.\n",
    "\n",
    "Remember that you should follow the general style guidelines for this course: well-named, short, focused functions with limited indentation using Markdown documentation that explains their implementation and the AI concepts behind them.\n",
    "\n",
    "This assignment sometimes wrecks havoc with IPython notebook, save often. Put your helper functions here, along with their documentation. There should be one Markdown cell for the documentation, followed by one Codecell for the implementation.  Additionally, you may want to start your programming in a regular text editor/IDE because RL **takes a long time to run**.\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**get_node_cost_from_world**\n",
    "\n",
    "Helper function to return the cost of a node in the world."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "def get_node_cost_from_world(node: Tuple[int, int], world: List[List[str]]) -> int:\n",
    "    \"\"\" Helper function to return the cost of a node in the world.\n",
    "\n",
    "    Args:\n",
    "        node: A given node in the world.\n",
    "        world: The world we are searching.\n",
    "\n",
    "    Returns:\n",
    "        Cost of a given node in the world.\n",
    "\n",
    "    \"\"\"\n",
    "    return COSTS[world[node[1]][node[0]]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "**valid**\n",
    "\n",
    "Function that returns whether or not a given x,y coordinate Tuple is valid"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "outputs": [],
   "source": [
    "def valid(next_action: Tuple[int, int], world: List[List[str]]) -> bool:\n",
    "    \"\"\" Function that returns whether or not a given x,y coordinate Tuple is valid\n",
    "\n",
    "    Args:\n",
    "        next_action: a Tuple representing an x,y coordinate\n",
    "        world: the world we are searching\n",
    "\n",
    "    Returns: true if valid, false otherwise\n",
    "\n",
    "    \"\"\"\n",
    "    dimension = len(world[0])\n",
    "    return 0 <= next_action[0] < dimension and 0 <= next_action[1] < dimension and \\\n",
    "           world[next_action[1]][next_action[0]] != 'x'"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n",
     "is_executing": false
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "-----"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "**argmax**\n",
    "\n",
    "Argmax function to return the arguments of the move that maximizes value."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "outputs": [],
   "source": [
    "def argmax(args: Dict[Tuple[int, int], Tuple[int, int]], q: Dict[Tuple[Tuple[int, int], Tuple[int, int]], float],\n",
    "           state: Tuple[int, int]) -> int:\n",
    "    \"\"\" Argmax function to return the arguments of the move that maximizes value.\n",
    "\n",
    "    This function determines the move that would provide the maximum value according\n",
    "    to the Q action-value dict.\n",
    "\n",
    "    If one or more moves provide an equal value, this function will randomly choose\n",
    "    a value to apply, to promote exploration.\n",
    "\n",
    "    Args:\n",
    "        args: Dict containing a move mapped to the state it would move to.\n",
    "        q: Action-value dict\n",
    "        state: The current state\n",
    "\n",
    "    Returns:\n",
    "        The index of the move to make in order to maximize value according to the action-value dict.\n",
    "\n",
    "    \"\"\"\n",
    "    max_args = args\n",
    "    max_value = -sys.maxsize\n",
    "    for k, v in args.items():\n",
    "        value = q[state, k]\n",
    "        if value > max_value:\n",
    "            # this move gives the maximum value\n",
    "            max_value = value\n",
    "            max_args = k\n",
    "        elif math.isclose(value, max_value):\n",
    "            # randomly swap when \"equal\"\n",
    "            if random.randint(0, 1) == 1:\n",
    "                max_value = value\n",
    "                max_args = k\n",
    "    # return the move\n",
    "    return CARDINAL_MOVES.index(max_args)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n",
     "is_executing": false
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "-----"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "**get_state**\n",
    "\n",
    "Helper function to return an x,y Tuple of the state after a move is applied."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "outputs": [],
   "source": [
    "def get_state(state: Tuple[int, int], move: Tuple[int, int]) -> Tuple[int, int]:\n",
    "    \"\"\" Helper function to return an x,y Tuple of the state after a move is applied.\n",
    "\n",
    "    Args:\n",
    "        state: the current state\n",
    "        move: the move to be applied to the current state\n",
    "\n",
    "    Returns:\n",
    "        The x,y Tuple containing the current state with the specified move applied.\n",
    "\n",
    "    \"\"\"\n",
    "    return state[0] + move[0], state[1] + move[1]"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n",
     "is_executing": false
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "-----"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "**get_probabilities_for_next_state**\n",
    "\n",
    "Helper function to create a list of probabilities that weight the next move."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "outputs": [],
   "source": [
    "def get_probabilities_for_next_state(state: Tuple[int, int], q: Dict[Tuple[Tuple[int, int], Tuple[int, int]], float],\n",
    "                                     world: List[List[str]]) -> List[float]:\n",
    "    \"\"\" Helper function to create a list of probabilities that weight the next move.\n",
    "\n",
    "    The \"best\" move to make is given a weight of 70%. The remaining 30% is split\n",
    "    evenly amongst the remaining valid moves. That is, if there are 3 other moves,\n",
    "    they each get 10%. If there are are 2 other moves, they each get 15%, and so on.\n",
    "\n",
    "    Args:\n",
    "        state: The current state.\n",
    "        q: The action-value dict.\n",
    "        world: The world we are searching.\n",
    "\n",
    "    Returns:\n",
    "        A list containing the probabilities that are assigned to each potential move.\n",
    "\n",
    "    \"\"\"\n",
    "    valid_moves = get_valid_moves_from_state(state, world)\n",
    "    best_move = argmax(valid_moves, q, state)\n",
    "\n",
    "    # initialize the four potential move options to 0\n",
    "    probabilities = [0] * 4\n",
    "\n",
    "    # set the valid moves to the minimum non-zero value\n",
    "    if len(valid_moves) > 1:\n",
    "        for k in valid_moves.keys():\n",
    "            probabilities[CARDINAL_MOVES.index(k)] = 0.3 / (len(valid_moves) - 1)\n",
    "\n",
    "    # set the best move to 70%\n",
    "    probabilities[best_move] = 0.7\n",
    "\n",
    "    # probabilities of next move in order down, right, up, left\n",
    "    return probabilities"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n",
     "is_executing": false
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "-----"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "**get_valid_moves_from_state**\n",
    "\n",
    "Helper function to provide a dict of valid moves from a given state."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "outputs": [],
   "source": [
    "def get_valid_moves_from_state(state: Tuple[int, int], world: List[List[str]]) -> Dict[\n",
    "    Tuple[int, int], Tuple[int, int]]:\n",
    "    \"\"\" Helper function to provide a dict of valid moves from a given state.\n",
    "\n",
    "    Args:\n",
    "        state: The provided state.\n",
    "        world: The world we are searching.\n",
    "\n",
    "    Returns:\n",
    "        A dict containing the valid moves from the given state and the new x,y Tuple that\n",
    "        they map to.\n",
    "\n",
    "    \"\"\"\n",
    "    valid_moves = {}\n",
    "    for move in CARDINAL_MOVES:\n",
    "        next_move = get_state(state, move)\n",
    "        if valid(next_move, world):\n",
    "            valid_moves[move] = get_state(state, next_move)\n",
    "    return valid_moves"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n",
     "is_executing": false
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "-----"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "**convert_action_utility_to_policy**\n",
    "\n",
    "Helper function that iterates through all of the information in our \"Q\" action-value dict and returns a policy."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "outputs": [],
   "source": [
    "def convert_action_utility_to_policy(q: Dict[Tuple[Tuple[int, int], Tuple[int, int]], float], world: List[List[str]],\n",
    "                                     goal: Tuple[int, int]) -> Dict[Tuple[int, int], Tuple[int, int]]:\n",
    "    \"\"\" Helper function that iterates through all of the information in our \"Q\" action-value dict and returns a policy.\n",
    "\n",
    "    This functions looks at every x,y coordinate in our world and assigns a \"correct\" move based on\n",
    "    the values in the Q dict.\n",
    "    \n",
    "    If there are mountains in the world, the policy assigns it a move of (1,1) which will tell the\n",
    "    print method to print a mountain.\n",
    "    \n",
    "    The goal state will be assigned a move of (2,2) to tell the print method to print a 'G' to\n",
    "    denote the goal state.\n",
    "    \n",
    "    Args:\n",
    "        q: The action-value Q dict.\n",
    "        world: The world we are searching.\n",
    "        goal: The goal state.\n",
    "\n",
    "    Returns:\n",
    "        A policy that contains the ideal move from every coordinate in the world.\n",
    "        \n",
    "    \"\"\"\n",
    "    policy = {}\n",
    "    # iterate over x, then y\n",
    "    for x in range(len(world[0])):\n",
    "        for y in range(len(world[0])):\n",
    "            if world[y][x] == 'x':\n",
    "                policy[(x, y)] = (1, 1)\n",
    "            elif (x, y) == goal:\n",
    "                policy[(x, y)] = (2, 2)\n",
    "            else:\n",
    "                # create a temporary dict containing the possible moves\n",
    "                possible_moves = {move: move for move in CARDINAL_MOVES}\n",
    "\n",
    "                # assign the move which has the highest value in the Q dict\n",
    "                policy[(x, y)] = CARDINAL_MOVES[argmax(possible_moves, q, (x, y))]\n",
    "\n",
    "    return policy"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n",
     "is_executing": false
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "-----"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "**q_learning**\n",
    "\n",
    "The Q-Learning function that creates a policy based on the provided parameters.\n",
    "\n",
    "* The function runs 100,000 episodes to determine its policy.\n",
    "\n",
    "* The function picks a random starting position for each episode, while making sure not to start on a mountain or in the goal state.\n",
    "\n",
    "* During each episode, if multiple \"best states\" have the same value, one is chosen at random, to promote exploration.\n",
    "\n",
    "* The \"best move\" is chosen %70 of the time, and the remaining 30% is divided equally amongst the other valid moves for that state. "
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "def q_learning(world: List[List[str]], costs: Dict[str, int], goal: Tuple[int, int], reward: int,\n",
    "               actions: List[Tuple[int, int]], gamma: float, alpha: float):\n",
    "    \"\"\" Q-learning function to generate a policy of moves.\n",
    "\n",
    "    This q-learning function is based off of the pseudo-code found in the\n",
    "    Barto and Sutton along with the pseudo-code provided in module 4.\n",
    "\n",
    "    The function creates a Q action value dictionary that initializes\n",
    "    all unknown keys to the value 100. It performs 100,000 \"episodes\" as\n",
    "    that number seemed to provide an adequate amount coverage to both the\n",
    "    small and large worlds.\n",
    "\n",
    "    The function starts exploring from a random spot on the map (provided\n",
    "    that it is not the goal and it's not in the mountains).\n",
    "\n",
    "    For each episode, the function loops until the goal is reached.\n",
    "\n",
    "    First, it finds the probabilities for the next state, and it chooses\n",
    "    one based on the weights determined by 'get_probabilities_for_next_state'.\n",
    "\n",
    "    The reward is then calculated, either as a gigantic goal reward, or as\n",
    "    a cost incurred by whatever terrain was chosen as the next_state.\n",
    "\n",
    "    Then the Q value for the current state is amended, according to the\n",
    "    equation provided: Q[s, a] := (1-α)Q[s, a] + α(r + ɣ max Q[s',a'])\n",
    "\n",
    "    If the goal was chosen as the next state, the episode ends, otherwise,\n",
    "    it continues looping until we find the goal state.\n",
    "\n",
    "    Finally, after all the episodes have been completed, the Q dict is\n",
    "    converted to a policy that dictates the \"correct\" move to make from\n",
    "    every valid point in the world.\n",
    "\n",
    "    Args:\n",
    "        world: the world we are searching\n",
    "        costs: a dict that defines the various \"costs\" associated with the\n",
    "            different types of terrain in our world\n",
    "        goal: a tuple representing the x,y coordinates of our goal node\n",
    "        reward: the reward for achieving the goal state\n",
    "        actions: a list of possible actions as offset tuples\n",
    "        gamma: the discount rate\n",
    "        alpha: the learning rate\n",
    "    Returns:\n",
    "        Dict containing the policy which has keys of x,y Tuples and the\n",
    "        action to perform, as an x,y offset Tuple.\n",
    "        \n",
    "    \"\"\"\n",
    "\n",
    "    q: Dict[Tuple[Tuple[int, int], Tuple[int, int]], float] = defaultdict(lambda: float(100))\n",
    "    for i in range(100_000):\n",
    "\n",
    "        # choose a random starting point\n",
    "        state = (random.randint(0, len(world[0]) - 1), random.randint(0, len(world[0]) - 1))\n",
    "\n",
    "        # don't let us randomly start on mountains or the goal\n",
    "        while world[state[1]][state[0]] == 'x' or state == goal:\n",
    "            state = (random.randint(0, len(world[0]) - 1), random.randint(0, len(world[0]) - 1))\n",
    "\n",
    "        # loop until you find the goal\n",
    "        while True:\n",
    "\n",
    "            # figure out our successor state\n",
    "            probabilities = get_probabilities_for_next_state(state, q, world)\n",
    "            action = actions[random.choices(range(4), probabilities)[0]]\n",
    "            next_state: Tuple[int, int] = (state[0] + action[0], state[1] + action[1])\n",
    "\n",
    "            # set the appropriate reward, either the cost of the move or the goal bonus\n",
    "            if next_state != goal:\n",
    "                episode_reward = get_node_cost_from_world(next_state, world)\n",
    "            else:\n",
    "                episode_reward = reward\n",
    "\n",
    "            next_action = actions[\n",
    "                argmax(get_valid_moves_from_state(next_state, world), q, next_state)]\n",
    "\n",
    "            q[(state, action)] += alpha * (\n",
    "                    (episode_reward + gamma * q[(next_state, next_action)]) - q[(state, action)])\n",
    "\n",
    "            # we reached the goal, end the episode\n",
    "            if next_state == goal:\n",
    "                break\n",
    "\n",
    "            # advance our state\n",
    "            state = next_state\n",
    "\n",
    "    # convert our Q dict to a policy of moves\n",
    "    return convert_action_utility_to_policy(q, world, goal)"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "-----"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "**pretty_print_policy**\n",
    "\n",
    "A helper function to print the results from the Q-Learning policy."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "def pretty_print_policy(cols: int, rows: int, policy: Dict[Tuple[int, int], Tuple[int, int]]):\n",
    "    \"\"\" Helper function to print the results from our policy.\n",
    "\n",
    "    This function has a dict that maps values in the policy to the proper character to be printed.\n",
    "    The various moves in the policy map to the appropriate arrows. The value (1,1) is a flag to\n",
    "    print mountains and the value (2,2) is a flag to print the goal state.\n",
    "\n",
    "    Args:\n",
    "        cols: The number of columns.\n",
    "        rows: The number of rows.\n",
    "        policy: The policy generated by our Q-learning method.\n",
    "\n",
    "    \"\"\"\n",
    "    arrows: Dict[Tuple[int, int], str] = {(0, -1): '^', (1, 0): '>', (0, 1): 'V', (-1, 0): '<', (1, 1): 'x',\n",
    "                                          (2, 2): 'G'}\n",
    "    for y in range(rows):\n",
    "        for x in range(cols):\n",
    "            print(arrows[policy[(x, y)]], end=\"\")\n",
    "        print(\"\")\n",
    "    print(\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "-----"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Small World\n",
    "\n",
    "### Outputs the non-deterministic Q-Learning policy for small.txt\n",
    "\n",
    "#### Goal reward of 10,000\n",
    "\n",
    "#### Discount rate (gamma) of 0.95\n",
    "\n",
    "#### Learning rate (alpha) of 0.6 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "small_world = read_world( \"small.txt\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "goal = (5, 5)\n",
    "reward = 10_000\n",
    "gamma = 0.95 # discount rate\n",
    "alpha = 0.6 # learning rate\n",
    "world = read_world(\"small.txt\")\n",
    "\n",
    "test_policy = q_learning(small_world, COSTS, goal, reward, CARDINAL_MOVES, gamma, alpha)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "text": [
      "VV>VVVV\n",
      "VVVVVVV\n",
      "VVVVVVV\n",
      "VV>>>V<\n",
      ">>VVVVV\n",
      ">>>>>G<\n",
      ">^x>>^<\n",
      "\n"
     ],
     "output_type": "stream"
    }
   ],
   "source": [
    "cols = len(world[0])\n",
    "rows = len(world[0])\n",
    "\n",
    "pretty_print_policy( cols, rows, test_policy)"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "-----"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Large World\n",
    "\n",
    "### Outputs the non-deterministic Q-Learning policy for large.txt\n",
    "\n",
    "#### Goal reward of 10,000,000\n",
    "\n",
    "#### Discount rate (gamma) of 0.95\n",
    "\n",
    "#### Learning rate (alpha) of 0.6 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "large_world = read_world( \"large.txt\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "goal = (26, 26) # Lower Right Corner\n",
    "reward = 10_000_000\n",
    "gamma = 0.95  # discount rate\n",
    "alpha = 0.6  # learning rate\n",
    "\n",
    "full_policy = q_learning(large_world, COSTS, goal, reward, CARDINAL_MOVES, gamma, alpha)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "text": [
      "V>>>>>>>>>>>>>>>>>>>>>>>>>V\n",
      "V>>>>>>>>>>>>>>>V^xxxxxxx>V\n",
      "VVV^xx>>>>>>>>>>VxxxVVVxxVV\n",
      "VVV<<xxxV>>>>>>>VV>>VV<xx>V\n",
      "VVV<<xxVVVV>>>>>>>>VVVxxx>V\n",
      "VVV<xx>VV>>>>>>>>>>>>VVxVVV\n",
      "VVVxx>>VV<^^xxx>>>>>>>>>>VV\n",
      "VVVVV>VVV<V^<<xxxVVV>>>V>>V\n",
      "VV>>>>VVV<<<<<xxVV>V>VVVV>V\n",
      "VV>>>>VVVV<xxxx>>VVV>>>>>>V\n",
      "VVV>V>VVV<xxx>>>>>VVxxx>V>V\n",
      "VV>>>>V>Vxx>>>>>>VVVVxx>VVV\n",
      "VV>>>>VVVxxVV>>>>>>VVxV>>VV\n",
      "VV>>>>VV>V>V>>>>>>>>>>>>VVV\n",
      "VV<^x>VV>>VV<>>>>>>>^^x>V>V\n",
      ">V<xxx>>>>>Vxxx>>>>^^xx>V>V\n",
      "VVxx>>>V>V>V>Vxxx^^xxxV>>>V\n",
      "VVVxx>>>V>>VVVVVxxxxV>>>VVV\n",
      "VVVxxx>V>>V>VVV>VV>V>>>>>>V\n",
      "VV>Vxxx>>>>>>>>V>V>VVVVVVVV\n",
      "VVVVVVxx>>>>>^xV>>VVVVVVVVV\n",
      "V>VVVVVxxx>^xx>>>>>VVVVVVVV\n",
      ">>>V>VVVVxxxx>>>>>>>VVVVVVV\n",
      ">>>>>VVVVVV>>>^^^xx>>VVVVVV\n",
      "Vx>>>>VVVVVxxx^^xxVxx>>VVVV\n",
      "VxxxV>V>>VVVxxxx>VVVxxxVVVV\n",
      ">>>>>>>>>>>>>>>>>>>>>>>>>>G\n",
      "\n"
     ],
     "output_type": "stream"
    }
   ],
   "source": [
    "cols = len(large_world[0])\n",
    "rows = len(large_world[0])\n",
    "\n",
    "pretty_print_policy( cols, rows, full_policy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Suggestions\n",
    "\n",
    "1. Implement Value Iteration...it's just a bunch of looping and it'll give you a sense of what the final policy would be like for Q Learning if you were able to run it infinitely, forever. It's also good partial credit.\n",
    "2. Turn off the stochasticity at the start. For the Large World, consider this...will the policy include the single (correct) path from A* search?\n",
    "3. Get things working on the small world first. It's also good partial credit."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----\n",
    "\n",
    "## Value Iteration (if submitting)\n",
    "\n",
    "### Value Iteration not implemented.\n",
    "\n",
    "Provide implementation and output of policy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "navigate_num": "#000000",
    "navigate_text": "#333333",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700",
    "sidebar_border": "#EEEEEE",
    "wrapper_background": "#FFFFFF"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "171px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false,
   "widenNotebook": false
  },
  "pycharm": {
   "stem_cell": {
    "cell_type": "raw",
    "source": [],
    "metadata": {
     "collapsed": false
    }
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
