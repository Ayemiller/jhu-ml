Training R-track.txt using value iteration for 40 iterations.
Crash Back to Start: False
Gamma=0.9, Epsilon=0.0001
Iteration 0:	max |Vt(s) - Vt-1(s)| = 1.0
Iteration 1:	max |Vt(s) - Vt-1(s)| = 0.8999999999999999
Iteration 2:	max |Vt(s) - Vt-1(s)| = 0.81
Iteration 3:	max |Vt(s) - Vt-1(s)| = 0.7290000000000001
Iteration 4:	max |Vt(s) - Vt-1(s)| = 0.6561000000000003
Iteration 5:	max |Vt(s) - Vt-1(s)| = 0.59049
Iteration 6:	max |Vt(s) - Vt-1(s)| = 0.531441
Iteration 7:	max |Vt(s) - Vt-1(s)| = 0.47829690000000014
Iteration 8:	max |Vt(s) - Vt-1(s)| = 0.43046721000000066
Iteration 9:	max |Vt(s) - Vt-1(s)| = 0.38742048900000103
Iteration 10:	max |Vt(s) - Vt-1(s)| = 0.3486784401000005
Iteration 11:	max |Vt(s) - Vt-1(s)| = 0.3138105960899997
Iteration 12:	max |Vt(s) - Vt-1(s)| = 0.28242953648100055
Iteration 13:	max |Vt(s) - Vt-1(s)| = 0.25418658283290085
Iteration 14:	max |Vt(s) - Vt-1(s)| = 0.2287679245496097
Iteration 15:	max |Vt(s) - Vt-1(s)| = 0.20589113209465015
Iteration 16:	max |Vt(s) - Vt-1(s)| = 0.18530201888518505
Iteration 17:	max |Vt(s) - Vt-1(s)| = 0.16677181699666477
Iteration 18:	max |Vt(s) - Vt-1(s)| = 0.1500946352969983
 --- Duration: 73.26 seconds ---
Average number of actions in time trial 1000.0
Iteration 19:	max |Vt(s) - Vt-1(s)| = 0.13508517176729917
 --- Duration: 77.6 seconds ---
Average number of actions in time trial 1000.0
Iteration 20:	max |Vt(s) - Vt-1(s)| = 0.12157665459056943
 --- Duration: 81.76 seconds ---
Average number of actions in time trial 1000.0
Iteration 21:	max |Vt(s) - Vt-1(s)| = 0.10941898913151249
 --- Duration: 86.09 seconds ---
Average number of actions in time trial 269.35
Iteration 22:	max |Vt(s) - Vt-1(s)| = 0.0984770902183616
 --- Duration: 90.29 seconds ---
Average number of actions in time trial 28.55
Iteration 23:	max |Vt(s) - Vt-1(s)| = 0.08862938119652419
 --- Duration: 94.58 seconds ---
Average number of actions in time trial 27.3
Iteration 24:	max |Vt(s) - Vt-1(s)| = 0.07823431538036729
 --- Duration: 98.84 seconds ---
Average number of actions in time trial 27.2
Iteration 25:	max |Vt(s) - Vt-1(s)| = 0.06632043166556656
 --- Duration: 103.05 seconds ---
Average number of actions in time trial 25.85
Iteration 26:	max |Vt(s) - Vt-1(s)| = 0.052500849046666076
 --- Duration: 107.23 seconds ---
Average number of actions in time trial 26.5
Iteration 27:	max |Vt(s) - Vt-1(s)| = 0.03889704636389091
 --- Duration: 112.99 seconds ---
Average number of actions in time trial 27.4
Iteration 28:	max |Vt(s) - Vt-1(s)| = 0.02701626959708925
 --- Duration: 117.24 seconds ---
Average number of actions in time trial 27.25
Iteration 29:	max |Vt(s) - Vt-1(s)| = 0.017620418927235093
 --- Duration: 121.51 seconds ---
Average number of actions in time trial 27.15
Iteration 30:	max |Vt(s) - Vt-1(s)| = 0.010804656808268476
 --- Duration: 125.79 seconds ---
Average number of actions in time trial 26.65
Iteration 31:	max |Vt(s) - Vt-1(s)| = 0.00617044303444203
 --- Duration: 129.96 seconds ---
Average number of actions in time trial 27.0
Iteration 32:	max |Vt(s) - Vt-1(s)| = 0.003372806699383446
 --- Duration: 134.22 seconds ---
Average number of actions in time trial 27.5
Iteration 33:	max |Vt(s) - Vt-1(s)| = 0.001754189667792616
 --- Duration: 138.7 seconds ---
Average number of actions in time trial 25.75
Iteration 34:	max |Vt(s) - Vt-1(s)| = 0.0008752236601949903
 --- Duration: 143.59 seconds ---
Average number of actions in time trial 26.7
Iteration 35:	max |Vt(s) - Vt-1(s)| = 0.00042292040269131803
 --- Duration: 147.85 seconds ---
Average number of actions in time trial 27.65
Iteration 36:	max |Vt(s) - Vt-1(s)| = 0.00019944592118648075
 --- Duration: 152.13 seconds ---
Average number of actions in time trial 27.8
Iteration 37:	max |Vt(s) - Vt-1(s)| = 9.23133432220169e-05
 --- Duration: 156.37 seconds ---
Average number of actions in time trial 26.2