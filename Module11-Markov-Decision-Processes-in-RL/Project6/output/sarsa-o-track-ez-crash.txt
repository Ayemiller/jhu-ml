Training O-track.txt using sarsa for 1000 iterations.
Crash Back to Start: False
Alpha=0.35, Gamma=0.9, Epsilon=0.05
 --- Duration: 1.68 seconds ---
Average number of actions in time trial 129.5

Training O-track.txt using sarsa for 10000 iterations.
Crash Back to Start: False
Alpha=0.35, Gamma=0.9, Epsilon=0.05
 --- Duration: 9.27 seconds ---
Average number of actions in time trial 8.0

Training O-track.txt using sarsa for 40000 iterations.
Crash Back to Start: False
Alpha=0.35, Gamma=0.9, Epsilon=0.05
 --- Duration: 23.15 seconds ---
Average number of actions in time trial 6.8

Training O-track.txt using sarsa for 60000 iterations.
Crash Back to Start: False
Alpha=0.35, Gamma=0.9, Epsilon=0.05
 --- Duration: 30.51 seconds ---
Average number of actions in time trial 6.3

Training O-track.txt using sarsa for 80000 iterations.
Crash Back to Start: False
Alpha=0.35, Gamma=0.9, Epsilon=0.05
 --- Duration: 37.23 seconds ---
Average number of actions in time trial 6.55