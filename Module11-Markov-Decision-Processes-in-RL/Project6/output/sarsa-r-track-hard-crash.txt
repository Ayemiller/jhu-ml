Training R-track.txt using sarsa for 1000 iterations.
Crash Back to Start: True
Alpha=0.35, Gamma=0.9, Epsilon=0.05
Reward count: 18
 --- Duration: 2.6 seconds ---
Average number of actions in time trial 500.0

Training R-track.txt using sarsa for 10000 iterations.
Crash Back to Start: True
Alpha=0.35, Gamma=0.9, Epsilon=0.05
Reward count: 158
 --- Duration: 20.19 seconds ---
Average number of actions in time trial 500.0

Training R-track.txt using sarsa for 40000 iterations.
Crash Back to Start: True
Alpha=0.35, Gamma=0.9, Epsilon=0.05
Reward count: 885
 --- Duration: 80.12 seconds ---
Average number of actions in time trial 480.15

Training R-track.txt using sarsa for 60000 iterations.
Crash Back to Start: True
Alpha=0.35, Gamma=0.9, Epsilon=0.05
Reward count: 14561
 --- Duration: 95.19 seconds ---
Average number of actions in time trial 59.4

Training R-track.txt using sarsa for 80000 iterations.
Crash Back to Start: True
Alpha=0.35, Gamma=0.9, Epsilon=0.05
Reward count: 29717
 --- Duration: 128.42 seconds ---
Average number of actions in time trial 40.65

Training R-track.txt using sarsa for 100000 iterations.
Crash Back to Start: True
Alpha=0.35, Gamma=0.9, Epsilon=0.05
Reward count: 45978
 --- Duration: 151.23 seconds ---
Average number of actions in time trial 46.3

Training R-track.txt using sarsa for 150000 iterations.
Crash Back to Start: True
Alpha=0.35, Gamma=0.9, Epsilon=0.05
Reward count: 84170
 --- Duration: 199.59 seconds ---
Average number of actions in time trial 45.9

Training R-track.txt using sarsa for 200000 iterations.
Crash Back to Start: True
Alpha=0.35, Gamma=0.9, Epsilon=0.05
Reward count: 125092
 --- Duration: 259.67 seconds ---
Average number of actions in time trial 43.55

Training R-track.txt using sarsa for 400000 iterations.
Crash Back to Start: True
Alpha=0.35, Gamma=0.9, Epsilon=0.05
Reward count: 274826
 --- Duration: 487.95 seconds ---
Average number of actions in time trial 48.3

Training R-track.txt using sarsa for 800000 iterations.
Crash Back to Start: True
Alpha=0.35, Gamma=0.9, Epsilon=0.05
Reward count: 581997
 --- Duration: 941.59 seconds ---
Average number of actions in time trial 37.8

Training R-track.txt using sarsa for 1500000 iterations.
Crash Back to Start: True
Alpha=0.35, Gamma=0.9, Epsilon=0.05
Reward count: 1122322
 --- Duration: 1676.22 seconds ---
Average number of actions in time trial 48.2