from __future__ import division

# def read_house_votes():
#     # split data into a training set, containing 2/3 of the data and a testing set, containing the remaining 1/3
#     rows = read_csv_file('house-votes-84.data')
#     training_set, test_set = np.array_split(rows, [int(len(rows) * .67)])
#     return training_set.tolist(), test_set.tolist()
#
#
# def read_csv_file(filename):
#     rows = []
#     # read in the data, replacing the single character domain with the full word
#     with open(filename, 'r') as csvfile:
#         csv_reader = csv.reader(csvfile)
#         for row in csv_reader:
#             rows.append(row)
#
#     # randomize the rows of input
#     random.shuffle(rows)
#
#     return rows


# replace missing values with the most frequent value for that column
# imputer = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
# data_set = pd.DataFrame(imputer.fit_transform(data_set))

# # find the mean of each column
# col_means = data_set.mean()


# # Set all values greater than mean, to 1. Set all values less than or equal to mean, to 0.
# for attribute_index in range(1, len(field_names)):
#     data_set[data_set.loc[:, data_set.columns == field_names[attribute_index]] <= col_means[attribute_index]] = 0
#     data_set[data_set.loc[:, data_set.columns == field_names[attribute_index]] > col_means[attribute_index]] = 1
