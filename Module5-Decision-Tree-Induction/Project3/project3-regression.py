import calendar

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


class BinaryNode:
    """A regression tree node."""

    def __init__(self, mse, classification):
        self.mse = mse
        self.classification = classification
        self.feature_index = 0
        self.threshold = 0
        self.left = None
        self.right = None


def print_tree(tree, field_names, class_names, prefix=""):
    """ Prints the regression tree in an ASCII format

    Args:
        tree: the binary regression tree
        prefix: parameter to help with spacing
    """

    if tree.right:
        # if not a leaf, print the feature and split value
        print(f'{prefix}|--{field_names[tree.feature_index]} < {tree.threshold}')
        prefix += "|  "
        if isinstance(tree, BinaryNode):
            # recurse by printing the left and right children
            if not tree.left == None:
                print_tree(tree.left, field_names, class_names, prefix)
            if not tree.right == None:
                print_tree(tree.right, field_names, class_names, prefix)
    else:
        # print the class of the leaf
        print(f'{prefix}|\n{prefix}|{class_names[tree.classification]}\n{prefix}|')


def learn(feature_values, class_values, mse_threshold=0):
    """ Learn the regression tree for the provided features and class values.

    Args:
        feature_values: The set of features from our training data.
        class_values:   The set of classes from our training data.
        mse_threshold:  The MSE early stopping threshold. The default value of zero
                        denotes no early stopping.
    """
    number_of_classes = len(set(class_values))
    number_of_features = feature_values.shape[1]
    return build_regression_tree(feature_values, class_values, number_of_classes, number_of_features, mse_threshold)


def mse(classes_values, mean_class):
    """
        Compute the mean squared error for a node.

    Args:
        class_values:   The classes of the node
        mean_class:     The mean classification of the node

    Returns: The mean squared error of the node.

    """
    number_of_classes = classes_values.size
    if number_of_classes == 0:
        return 0
    error_sum = 0.0
    for value in classes_values:
        if value != mean_class:
            error_sum += 1
    mean_squared_error = error_sum / number_of_classes
    return mean_squared_error


def pick_best_split(feature_values, classes_values, number_of_classes, number_of_features):
    """Function that implements the split logic of the CART algorithm based
    using mean squared error (MSE) as the split. It chooses the feature to
    split on that creates the lowest mean squared error of the resulting
    two sets of data.
    Returns:
        best_feature_index: Index of the feature for best split, or None if no split is found.
        best_split_value: Threshold to use for the split, or None if no split is found.
    """
    # Need at least two elements to split a node.
    classes_len = classes_values.size
    sorted_feature_values, sorted_class_values = zip(*sorted(zip(feature_values[:, 0], classes_values)))
    if classes_len <= 1:
        return None, None
    # Count of each class in the current node.
    num_parent = [np.sum(classes_values == class_index) for class_index in range(number_of_classes)]

    predicted_class = int(round(sum(np.array(sorted_class_values)))) / classes_len
    mean_squared_error = mse(classes_values, predicted_class)

    best_feature_index, best_split_value = None, None
    best_mse = mean_squared_error

    # Iterate through all of the features
    for feature_index in range(number_of_features):

        # Sort the features in ascending order along with their associated classes
        sorted_feature_values, sorted_class_values = zip(*sorted(zip(feature_values[:, feature_index], classes_values)))

        class_count_left = [0] * number_of_classes
        class_count_right = num_parent.copy()
        # Iterate through the possible splits based on the classes of our sorted features
        for split_index in range(1, classes_len):
            class_of_split = sorted_class_values[split_index - 1]

            # if a dataset doesn't contain a class contained in another set, just ignore it
            if class_of_split > len(class_count_left) - 1:
                continue
            class_count_left[class_of_split] += 1

            # calculate the predicted class and MSE of the left child
            left_class_sum = sum(x * class_count_left[x] for x in range(len(class_count_left)))
            if sum(class_count_left) == 0:
                predicted_class_left = 0
            else:
                predicted_class_left = round(int(left_class_sum / sum(class_count_left)))
            left_list = []
            for index in range(len(class_count_left)):
                for _ in range(class_count_left[index]):
                    left_list.append(index)
            left_mse = mse(np.array(left_list), predicted_class_left)
            class_count_right[class_of_split] -= 1

            # calculate the predicted class and MSE of the right child
            right_class_sum = sum(x * class_count_right[x] for x in range(len(class_count_right)))
            if sum(class_count_right) == 0:
                predicted_class_right = 0
            else:
                predicted_class_right = round(int(right_class_sum / sum(class_count_right)))
            right_list = []
            for index in range(len(class_count_right)):
                for _ in range(class_count_right[index]):
                    right_list.append(index)
            right_mse = mse(np.array(right_list), predicted_class_right)

            weighted_mean_squared_error = (left_mse * split_index + right_mse * (
                    classes_len - split_index)) / classes_len

            # don't allow a split between two features with the same value
            if sorted_feature_values[split_index] == sorted_feature_values[split_index - 1]:
                continue

            # test the new MSE to see if it is better
            if weighted_mean_squared_error < best_mse:
                best_mse = weighted_mean_squared_error
                best_feature_index = feature_index
                # choose the midpoint of our sorted features as the split
                best_split_value = (sorted_feature_values[split_index] + sorted_feature_values[split_index - 1]) / 2

    return best_feature_index, best_split_value


def build_regression_tree(feature_values, class_values, number_of_classes, number_of_features, mse_threshold=0):
    """ Create regression tree using the CART algorithm. Continue splitting on the feature
        and value that minimize the mean squared error (MSE) until the provided threshold
        is satisfied. If no threshold is provided, splitting will continue until all
        leaves are homogeneous.

    Args:
        feature_values: The list of features
        class_values:   The list of classes
        number_of_classes:  The number of classes
        number_of_features: The number of features
        mse_threshold:  The MSE early stopping threshold. The default value of zero
                        denotes no early stopping.
    Returns:
        The regression tree for the provided data
    """
    num_samples_per_class = [np.sum(class_values == i) for i in range(number_of_classes)]
    mean_class = int(sum(class_values) / class_values.size)

    node = BinaryNode(
        mse=mse(class_values, mean_class),
        classification=mean_class,
    )

    # Keep splitting until the node's mean squared error is below the threshold
    if node.mse > mse_threshold:
        feature_index, feature_value = pick_best_split(feature_values, class_values, number_of_classes,
                                                       number_of_features)

        if feature_index is not None:
            indices_left = feature_values[:, feature_index] < feature_value
            features_left, classes_left = feature_values[indices_left], class_values[indices_left]
            features_right, classes_right = feature_values[~indices_left], class_values[~indices_left]
            node.feature_index = feature_index
            node.threshold = feature_value
            node.left = build_regression_tree(features_left, classes_left, number_of_classes, number_of_features,
                                              mse_threshold)
            node.right = build_regression_tree(features_right, classes_right, number_of_classes, number_of_features,
                                               mse_threshold)
    return node


def normalize(dataframe, start_column=0, end_column=-1, span_of_values=1):
    """ Normalize the floating point values using max-min normalization.

    Args:
        dataframe: The dataset we are normalizing.
        end_column: First column to normalize
        start_column: Last column to normalize
        span_of_values: The number of values that the normalization should span.
                        A value of 1 means from 0 to 1. A value of 2 means form 0 to 2, etc.
    Returns:
        The normalized dataframe

    """
    # shuffle the rows
    dataframe = dataframe.sample(frac=1)

    # default end column is second to last, so as not to normalize the class
    if end_column == -1:
        end_column = len(dataframe.columns) - 2

    for index in range(start_column, end_column + 1):
        column = dataframe.columns[index]
        dataframe[column] = dataframe[column].apply(pd.to_numeric)
        max_value = dataframe[column].max()
        min_value = dataframe[column].min()

        # perform min-max normalization
        dataframe[column] = (dataframe[column] - min_value) / ((max_value - min_value) + 0.00000001) * span_of_values
    return dataframe


def classify(tree, test_items):
    """ Returns a list of classifications for the provided data using the provided decision tree.

    Args:
        tree: The provided regression tree.
        test_items: The provided test data.

    Returns:
        A list of containing the classifications for a test set against the regression tree tree.
    """
    classifications = []
    for item in test_items:
        output_from_classification = single_classification(tree, item[:-1])
        classifications.append(output_from_classification)
    return classifications


def single_classification(tree, test_item):
    """ 'Classify' a single test item using the regression tree.

    Args:
        tree: The regression tree.
        inputs: Test items

    Returns:
        The 'classification' of the test item, according to the regression tree.
    """
    node = tree
    while node.left:
        if test_item[node.feature_index] < node.mse:
            node = node.left
        else:
            node = node.right
    return node.classification


def evaluate(test_data, classifications):
    """ This function compares the test_data classifications to the created classifications and returns the error-rate.

    Args:
        test_data: Test data from a classification dataset
        classifications: Generated classifications from our decision tree.

    Returns:

    """
    test_classes = [row[0] for row in test_data]
    errors = 0
    for item1, item2 in zip(test_classes, classifications):
        # skip blank classifications, due to one dataset containing unique items
        if item1 != item2 and item1 and item2:
            errors += 1
    return errors / len(test_data) * 100


def perform_cart(folds, validation_set, field_names, class_names, mse_threshold=0):
    """ Helper function to learn/test/output our cart algorithm.

    Args:
        folds: The training/test folds
        validation_set: The validation dataset
        field_names: The field labels for output purposes
        class_names: The class labels for output purposes
        mse_threshold:  The MSE early stopping threshold. The default value of zero
                        denotes no early stopping.
    """
    total_test_instances = 0
    error_rate = []
    for test_fold_index in range(0, len(folds)):
        test_data = folds[test_fold_index].tolist()
        total_test_instances += len(test_data)

        # set the remaining four folds to be training data
        training_data = np.concatenate(np.delete(folds, test_fold_index, axis=0))

        features = training_data[:, :-1]
        classes = training_data[:, -1]
        regression_tree = learn(features, classes, mse_threshold)

        if verbose:
            print(f'\nRegression tree {test_fold_index + 1}:')
            print_tree(regression_tree, field_names,
                       class_names)
        classifications = classify(regression_tree, test_data)
        error_rate.append(evaluate(test_data, classifications))
        if verbose:
            print(f'Error Rate {test_fold_index + 1}:\t\t\t' + '{:05.2F}%'.format(error_rate[test_fold_index]))
    avg_error_rate = sum(error_rate) / len(error_rate)
    print(f'\nAvg Error Rate: {avg_error_rate:.2f}%\n')
    return avg_error_rate


verbose = False

# ************************************************************************************
# COMPUTER HARDWARE (regression)
# ************************************************************************************
print('\nTraining and testing computer hardware dataset.')
machine_csv = pd.read_csv('datasets/machine (1).data', header=None)

# delete columns 0 and 1
machine_csv.drop(machine_csv.columns[[0, 1]], axis=1, inplace=True)

# delete the "ERP", or provided regression column, but we'll keep it for later
provided_regression_prediction = machine_csv.pop(9).to_numpy()

# normalize features so that they are between 0 and 1
machine_csv = normalize(machine_csv, 0, 5, 1)

# normalize class so that it is between 0 and 2
machine_csv = normalize(machine_csv, 6, 6, 2)

# round the class to the nearest of 0, 1 or 2
machine_csv = machine_csv.round({8: 0})
machine_csv = machine_csv.convert_dtypes()

# shuffle data
machine_csv = machine_csv.sample(frac=1)

test_train_set, validation_set = np.array_split(machine_csv.to_numpy(), [int(len(machine_csv) * .9)])
validation_set = validation_set.tolist()

field_names = ['MYCT', 'MMIN', 'MMAX', 'CACH', 'CHMIN', 'CHMAX']
class_names = ['low-performance', 'med-performance', 'high-performance']

# dict w/ MSE value as key and average MSE of classification as value
machine_avg_error_rates = {}

# perform cart algorithm ten times using MSE early stopping from 0 (disabled), to 0.2 in 0.2 increments
for mse_incrementor in range(0, 10):
    mse_value = 0 + mse_incrementor * .02
    machine_avg_error_rates[mse_value] = perform_cart(np.array_split(test_train_set, 5), validation_set,
                                                      field_names, class_names, mse_value)
plt.figure(figsize=(10, 7))
plt.plot(list(machine_avg_error_rates.keys()), list(machine_avg_error_rates.values()), color='r',
         label='CPU Performance')
plt.legend()
plt.xlabel('Early Stopping Threshold')
plt.ylabel('MSE of Classification')
plt.title('CPU CART Early Stopping Performance', fontsize=16, weight='heavy')
plt.savefig('CPU-CART-Early-Stopping-Performance' + '.png')
plt.show()
print()

# ************************************************************************************
# FOREST FIRES (regression)
# ************************************************************************************
print('\nTraining and testing forest fire dataset.')
forestfires_csv = pd.read_csv('datasets/forestfires.data')

# transform months from month to numeric
forestfires_csv['month'] = forestfires_csv['month'].apply(
    lambda x: [month.lower() for month in list(calendar.month_abbr)][1:].index(x) + 1)

# transform days from mon-to-sun to numeric
forestfires_csv['day'] = forestfires_csv['day'].apply(
    lambda x: [day.lower() for day in list(calendar.day_abbr)].index(x) + 1)

# normalize the features so that they are between 0 and 1
forestfires_csv = normalize(forestfires_csv)

# normalize class so that it is between 0 and 2
forestfires_csv = normalize(forestfires_csv, 12, 12, 2)

# round the class to the nearest of 0, 1 or 2
forestfires_csv = forestfires_csv.round({'area': 0})
forestfires_csv = forestfires_csv.convert_dtypes()

# shuffle data
forestfires_csv = forestfires_csv.sample(frac=1)

test_train_set, validation_set = np.array_split(forestfires_csv.to_numpy(), [int(len(forestfires_csv) * .9)])
validation_set = validation_set.tolist()

field_names = forestfires_csv.columns.tolist()[:-1]
class_names = ['small-area', 'med-area', 'large-area']

# dict w/ MSE value as key and average MSE of classification as value
forestfires_avg_error_rates = {}

# perform cart algorithm ten times using MSE early stopping from 0 (disabled), to 0.2 in 0.2 increments
for mse_incrementor in range(0, 10):
    mse_value = 0 + mse_incrementor * .02
    forestfires_avg_error_rates[mse_value] = perform_cart(np.array_split(test_train_set, 5),
                                                          validation_set,
                                                          field_names, class_names,
                                                          mse_value)
plt.figure(figsize=(10, 7))
plt.plot(list(forestfires_avg_error_rates.keys()), list(forestfires_avg_error_rates.values()), color='r',
         label='Forest Fire Performance')
plt.legend()
plt.xlabel('Early Stopping Threshold')
plt.ylabel('MSE of Classification')
plt.title('Forest Fire CART Early Stopping Performance', fontsize=16, weight='heavy')
plt.savefig('Forest-Fire-CART-Early-Stopping-Performance' + '.png')
plt.show()
print()

# ************************************************************************************
# WINE QUALITY (RED) (regression)
# ************************************************************************************
print('\nTraining and testing wine quality (red) dataset.')
winequality_csv = pd.read_csv('datasets/winequality-red.csv', sep=';')

# normalize the features so that they are between 0 and 1
winequality_csv = normalize(winequality_csv)

# normalize class so that it is between 0 and 2
winequality_csv = normalize(winequality_csv, 11, 11, 2)

# round the class to the nearest of 0, 1 or 2
winequality_csv = winequality_csv.round({'quality': 0})
winequality_csv = winequality_csv.convert_dtypes()

# shuffle data
winequality_csv = winequality_csv.sample(frac=1)

test_train_set, validation_set = np.array_split(winequality_csv.to_numpy(), [int(len(winequality_csv) * .4)])
validation_set = validation_set.tolist()

field_names = winequality_csv.columns.tolist()[:-1]
class_names = ['0', '1', '2']

# dict w/ MSE value as key and average MSE of classification as value
winequality_avg_error_rates = {}

# perform cart algorithm ten times using MSE early stopping from 0 (disabled), to 0.3 in 0.5 increments
for mse_incrementor in range(0, 9):
    mse_value = 0 + mse_incrementor * 0.1
    winequality_avg_error_rates[mse_value] = perform_cart(np.array_split(test_train_set, 5),
                                                          validation_set,
                                                          field_names, class_names,
                                                          mse_value)
plt.figure(figsize=(10, 7))
plt.plot(list(winequality_avg_error_rates.keys()), list(winequality_avg_error_rates.values()), color='r',
         label='Wine Quality (red) Performance')
plt.legend()
plt.xlabel('Early Stopping Threshold')
plt.ylabel('MSE of Classification')
plt.title('Wine Quality (Red) CART Early Stopping Performance', fontsize=16, weight='heavy')
plt.savefig('Wine-Quality-red-CART-Early-Stopping-Performance' + '.png')
plt.show()
print()

# ************************************************************************************
# WINE QUALITY (WHITE) (regression)
# ************************************************************************************
print('\nTraining and testing wine quality (white) dataset.')
whitewinequality_csv = pd.read_csv('datasets/winequality-white.csv', sep=';')

# normalize the features so that they are between 0 and 1
whitewinequality_csv = normalize(whitewinequality_csv)

# normalize class so that it is between 0 and 2
whitewinequality_csv = normalize(whitewinequality_csv, 11, 11, 2)

# round the class to the nearest of 0, 1 or 2
whitewinequality_csv = whitewinequality_csv.round({'quality': 0})
whitewinequality_csv = whitewinequality_csv.convert_dtypes()

# shuffle data
whitewinequality_csv = whitewinequality_csv.sample(frac=1)

test_train_set, validation_set = np.array_split(whitewinequality_csv.to_numpy(), [int(len(whitewinequality_csv) * .15)])
validation_set = validation_set.tolist()

field_names = whitewinequality_csv.columns.tolist()[:-1]
class_names = ['0', '1', '2']

# dict w/ MSE value as key and average MSE of classification as value
whitewinequality_avg_error_rates = {}

# perform cart algorithm ten times using MSE early stopping from 0 (disabled), to 0.3 in 0.5 increments
for mse_incrementor in range(0, 9):
    mse_value = 0 + mse_incrementor * 0.1
    whitewinequality_avg_error_rates[mse_value] = perform_cart(np.array_split(test_train_set, 5),
                                                               validation_set,
                                                               field_names, class_names,
                                                               mse_value)
plt.figure(figsize=(10, 7))
plt.plot(list(whitewinequality_avg_error_rates.keys()), list(whitewinequality_avg_error_rates.values()), color='r',
         label='Wine Quality (White) Performance')
plt.legend()
plt.xlabel('Early Stopping Threshold')
plt.ylabel('MSE of Classification')
plt.title('Wine Quality (White) CART Early Stopping Performance', fontsize=16, weight='heavy')
plt.savefig('Wine-Quality-white-CART-Early-Stopping-Performance' + '.png')
plt.show()
print()
