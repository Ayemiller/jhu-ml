from __future__ import division  # so that 1/2 = 0.5 and not 0

import itertools
import math
from collections import defaultdict
from copy import deepcopy

import numpy as np
import pandas as pd


class Node(object):
    """ Class to represent nodes for our decision tree.

    Attributes:
        attribute (str): Description of this node
        children (:obj:`dict` of str or :obj:`Node`): Children of this node

    """

    def __init__(self, attribute):
        self.attribute = attribute
        self.children = {}
        self.parent = None
        self.pruned = False

    def __str__(self):
        return self.attribute


def is_homogenous(data):
    """ Helper function that indicates whether or not the provided data is homogeneous.

    Args:
        data: The provided data set.

    Returns:
        Returns true of the data is homogeneous, false otherwise.

    """
    class_label = data[0][0]
    for index in range(0, len(data)):
        if (data[index][0] != class_label):
            return False
    return True


def majority_label(data):
    """ Function that returns the majority label for the provided data.

    Args:
        data: The provided data set

    Returns:
        Returns the string label that accounts for the majority of the provided data set.

    """
    labels = defaultdict(lambda: 0)
    for entry in data:
        # increment count of each entry
        labels[entry[0]] += 1
    max_label = '?'
    max_count = -1
    for key in labels.keys():
        if labels[key] > max_count:
            max_label = key
            max_count = labels[key]
    return max_label


def pick_best_attribute(data, attributes):
    """ This function chooses the best attribute based on 'information gain ratio'
        as described in the module 5 lectures.

    Args:
        data: The provided data
        attributes: The provided attributes

    Returns:
        Returns the name of the attribute with the highest information gain.

    """
    # if we are passing only the class and an attribute, return the attribute
    if len(attributes) == 2:
        return attributes[1]

    # loop through class to calculate E(S), the starting entropy
    starting_entropy = 0.0
    class_dict = defaultdict(lambda: 0)
    for sample in data:
        class_value = sample[0]
        class_dict[class_value] += 1
    for clazz, count in class_dict.items():
        starting_entropy += -1 * count / len(data) * math.log(count / len(data), 2)

    attr_dict = {}

    # loop through each -attribute- to get class counts for each attribute value
    for attribute_index in range(1, len(attributes)):
        value_dict = {}
        # loop through each sample in dataset
        for sample in data:
            attribute_value = sample[attribute_index]
            class_value = sample[0]
            if not value_dict.get(attribute_value):
                value_dict[attribute_value] = defaultdict(lambda: 0)
            value_dict[attribute_value][class_value] += 1
        attr_dict[attribute_index] = value_dict

    # calc the entropies of each attribute
    attribute_gain_ratio = {}
    for attribute_index, value_dict in attr_dict.items():
        attribute_entropy_sum = 0.0
        total_count = 0
        intrinsic_value = 0.0

        for attribute, class_dict in value_dict.items():
            individual_entropy = 0.0
            total = sum(class_dict.values())
            total_count += total
            for clazz, count in class_dict.items():
                individual_entropy += -1 * count / total * math.log(count / total, 2) * total

            attribute_entropy_sum += individual_entropy
        attribute_entropy_sum /= total_count
        information_gain = starting_entropy - attribute_entropy_sum

        # calculate the intrinsic value (IV)
        attribute_domain = domain_of(attributes[attribute_index], data, attributes)
        for attribute_value in attribute_domain:
            subset = get_subset_of_data(attribute, attributes[attribute_index], data, attributes)
            attribute_count = len(subset)
            if attribute_count == 0:
                intrinsic_value = 0
            else:
                intrinsic_value += -1 * attribute_count / total_count * math.log(attribute_count / total_count, 2)

        # calculate the gain ratio
        if intrinsic_value == 0:
            information_gain_ratio = 0
        else:
            information_gain_ratio = information_gain / intrinsic_value
        attribute_gain_ratio[attribute_index] = information_gain_ratio

    # return the attribute that provides the highest information gain
    return attributes[max(attribute_gain_ratio, key=attribute_gain_ratio.get)]


def domain_of(attribute, data, attributes):
    """ Returns the domain of an given attribute

    Args:
        attribute: The attribute who's domain we are searching for
        data: The set of data we're searching
        attributes: The set of attributes in our data

    Returns:
        List containing the domain of a given attribute.

    """
    domain = defaultdict(lambda: 0)
    index = attributes.index(attribute)
    for entry in data:
        domain[entry[index]] += 1
    # sort the domain by number of each element, greatest to least
    return [i[1] for i in sorted(((value, key) for (key, value) in domain.items()), reverse=True)]


def get_subset_of_data(value, attribute, data, attributes):
    """ Function to return a subset of data that contains a specific value for an attribute.

    Args:
        value: The specified value our subset should have.
        attribute: The specified attribute for the value.
        data: The data set from which we will create a subset.
        attributes: The set of all attributes.

    Returns:
        A subset of only the data which contains a specified value for a specified attribute.

    """
    index = attributes.index(attribute)
    subset = []
    for entry in data:
        if entry[index] == value:
            subset.append(entry)
    subset = deepcopy(subset)
    [row.pop(index) for row in subset]
    return subset


def print_tree(tree, prefix=""):
    """ Prints the decision tree in an ASCII format

    Args:
        tree: the decision tree
        prefix: parameter to help with spacing
    """
    print(f'{prefix}|--{tree}')
    prefix += "|  "
    if isinstance(tree, Node):
        for child_key, child_value in tree.children.items():
            if isinstance(child_key, str):
                print(f'{prefix}|\n{prefix}|{child_key}\n{prefix}|')
            print_tree(child_value, prefix)


def id3(data, attributes, default):
    """ Function that implements the ID3 algorithm as presented in the provided pseudocode.

    Returns:
        Returns the classification tree that is generated from the provided data.
    """
    # if not data:
    #     return default
    if is_homogenous(data):
        return data[0][0]
    if not attributes or len(data[0]) == 1:
        return majority_label(data)
    best_attribute = pick_best_attribute(data, attributes)
    node = Node(best_attribute)
    default_label = majority_label(data)

    for value in domain_of(best_attribute, data, attributes):
        subset = get_subset_of_data(value, best_attribute, data, attributes)
        recurse_attributes = deepcopy(attributes)
        recurse_attributes.remove(best_attribute)
        child = id3(subset, recurse_attributes, default_label)
        if isinstance(child, Node):
            child.parent = node
        node.children[value] = child

    return node


def reduced_error_pruning(tree, attributes, data):
    """ Traverse the tree starting at the leaves and attempt to replace
        a node with the modal class. Keep the replacement if it does not
        decrease the accuracy.

    Args:
        tree: The decision tree we are pruning
        data: Our validation set

    Returns:
        The pruned tree.
    """
    return recursively_prune(tree, tree, attributes, data, data)


def get_root(node):
    while node.parent is not None:
        node = node.parent
    return node


def recursively_prune(parent, root_node, attributes, data, subset):
    """ Perform reduced error pruning for the given tree. All of the nodes of the tree are replaced with the majority
        label in the tree and are retested with a provided data set. If the accuracy of the modified tree is not
        reduced, the substitution is kept; otherwise it is reverted.

    Args:
        parent: The current node of the tree
        root_node: The root node of the tree
        attributes: The attributes of the data set
        data: The full data set
        subset: The subset of data, used as we recurse through the tree

    Returns:
        The pruned tree.
    """
    if parent.pruned:
        return parent
    else:
        children = parent.children
        for child_name, child_value in children.items():
            if isinstance(child_value, Node) and not child_value.pruned:
                child_subset = get_subset_of_data(child_name, parent.attribute, data, attributes)
                current_root = deepcopy(get_root(parent))
                prior_accuracy = evaluate(data, classify(current_root, data))
                global PRUNES
                PRUNES += 1
                prune_result = recursively_prune(child_value, root_node, attributes, data, child_subset)
                prior_node = deepcopy(parent.children[child_name])
                current_root = get_root(parent)
                post_accuracy = evaluate(data, classify(current_root, data))
                if post_accuracy > prior_accuracy:
                    # revert pruning
                    parent.children[child_name] = prior_node
                else:
                    parent.children[child_name] = prune_result
                parent.pruned = True
                return parent

        # try to prune this node
        if isinstance(parent, Node) and not parent.pruned and parent is not root_node and subset:
            prior_accuracy = evaluate(data, classify(root_node, data))

            # replace old node w/ pruned value
            most_frequent_value = get_mode_for_node('class', attributes, subset)
            return most_frequent_value
        else:
            return get_root(parent)

        return parent


def get_mode_for_node(tree, attributes, data):
    """ Returns the modal value in a data set for a given attribute of a tree.

    Args:
        tree: The tree we are searching
        attributes: The attribute whose values we are searching
        data: The data set we are searching

    Returns:
        The modal value of a data set of a certain attribute.
    """
    class_counts = defaultdict(lambda: 0)
    attribute = attributes.index(tree)
    for item in data:
        class_counts[item[attribute]] += 1
    max_class = max(class_counts, key=class_counts.get)
    return max_class


def recursively_classify(tree, item):
    """ Traverse the tree using the provided input item until we reach a leaf.

    Args:
        tree: The provided decision tree.
        item: The provided input data.

    Returns:
        The classification for the input item.
    """

    if isinstance(tree, Node):
        item_to_remove = item[field_names.index(tree.attribute)]

        try:
            newtree = tree.children[item_to_remove]
        except KeyError:
            # Occasionally, I would see a tree that did not contain a permutation in the other, resulting in a KeyError.
            # When this happens, I will classify it as a blank string so that it is ignored.
            return ''

        return recursively_classify(newtree, item)
    else:
        return tree


def view(tree):
    """ Prints the decision tree.

    Args:
        tree: the decision tree
    """
    print_tree(tree)


def classify(tree, test_data):
    """ Returns a list of classifications for the provided data using the provided decision tree.

    Args:
        tree: The provided decision tree.
        test_data: The provided test data.

    Returns:
        A list of strings containing the classifications for a test set against a decision tree.
    """
    classifications = []
    for item in test_data:
        output_from_classification = recursively_classify(tree, item)
        classifications.append(output_from_classification)
    return classifications


def evaluate(test_data, classifications):
    """ This function compares the test_data classifications to the created classifications and returns the error-rate.

    Args:
        test_data: Test data from a classification dataset
        classifications: Generated classifications from our decision tree.

    Returns:

    """
    test_classes = [row[0] for row in test_data]
    errors = 0
    for item1, item2 in zip(test_classes, classifications):
        # skip blank classifications, due to one dataset containing unique items
        if item1 != item2 and item1 and item2:
            errors += 1
    return errors / len(test_data) * 100


def perform_id3(folds, validation_set, field_names):
    """Helper function to learn/test/output the ID3 algorithm.

    Args:
        folds: the test/train folds
        validation_set: the validation dataset
        field_names: the field names, used for output
    """
    total_test_instances = 0
    error_rate = []
    pruned_error_rate = []

    # perform cross validation for the folds
    for test_fold_index in range(0, len(folds)):
        global PRUNES
        PRUNES = 0

        # set one of the folds to be test data
        test_data = folds[test_fold_index].tolist()
        total_test_instances += len(test_data)
        # set the remaining four folds to be training data
        training_data = np.concatenate(np.delete(folds, test_fold_index, axis=0)).tolist()

        # create the id3 decision tree using the training set
        decision_tree = id3(training_data, field_names, '')
        if verbose:
            print(f'\nDecision tree {test_fold_index + 1}:')
            view(decision_tree)
        # test the id3 decision tree
        classifications = classify(decision_tree, test_data)
        # get the id3 error rate
        error_rate.append(evaluate(test_data, classifications))

        # create pruned decision tree
        pruned_decision_tree = reduced_error_pruning(decision_tree, field_names, validation_set)
        if verbose:
            print(f'\nPruned Decision tree {test_fold_index + 1}:')
            view(pruned_decision_tree)
        # test the pruned decision tree
        pruned_classifications = classify(pruned_decision_tree, test_data)
        # get the error rate for the pruned decision tree
        pruned_error_rate.append(evaluate(test_data, pruned_classifications))

        # append the error rates to running totals to later create averages
        print(f'Error Rate {test_fold_index + 1}:\t\t\t' + '{:05.2F}%'.format(error_rate[test_fold_index]))
        print(f'Pruned Error Rate {test_fold_index + 1}:\t' + '{:05.2F}%'.format(pruned_error_rate[test_fold_index]))
        print(f'Number of nodes pruned: {PRUNES}')

    # Print the average error rates for both normal id3 and pruned
    print(f'\nAvg Error Rate: {sum(error_rate) / len(error_rate):.2f}%')
    print(f'Avg Pruned Error Rate: {sum(pruned_error_rate) / len(pruned_error_rate):.2f}%')


def normalize(dataframe, start_column, end_column, span_of_values):
    """ Normalize the floating point values using max-min normalization

    Args:
        dataframe: The dataset we are normalizing.
        end_column: First column to normalize
        start_column: Last column to normalize
        span_of_values: The number of values that the normalization should span.
                        A value of 1 means from 0 to 1. A value of 2 means form 0 to 2, etc.
    Returns:
        The normalized dataframe

    """
    # shuffle the rows
    dataframe = dataframe.sample(frac=1)

    for index in range(start_column, end_column + 1):
        column = dataframe.columns[index]
        dataframe[column] = dataframe[column].apply(pd.to_numeric)
        max_value = dataframe[column].max()
        min_value = dataframe[column].min()

        # perform min-max normalization
        dataframe[column] = (dataframe[column] - min_value) / (
                (max_value - min_value) + 0.00000001) * span_of_values  # * 10 + 1
    return dataframe


PRUNES = 0
##########################################################################################
# car
##########################################################################################
print('Training and testing car dataset.')
field_names = ['buying-price', 'maint-price', 'num-doors', 'num-persons', 'cargo-vol', 'safety', 'class']
car_data = pd.read_csv('datasets/car.data', names=field_names)

# move the class label from the last column, to the first
field_names.insert(0, field_names.pop(field_names.index('class')))
car_data = car_data.reindex(columns=field_names)
car_data = car_data.sample(frac=1)
test_train_set, validation_set = np.array_split(car_data.to_numpy(), [int(len(car_data) * .9)])
validation_set = validation_set.tolist()
verbose = True
perform_id3(np.array_split(test_train_set, 5), validation_set, field_names)

##########################################################################################
# abalone
##########################################################################################
verbose = False
print('\nTraining and testing abalone dataset.')
field_names = ['sex', 'length', 'diameter', 'height', 'whole-weight', 'shucked-weight', 'viscera-weight',
               'shell-weight', 'class']

abalone_data = pd.read_csv('datasets/abalone (1).data', names=field_names)

# move the class label from the last column, to the first
field_names.insert(0, field_names.pop(field_names.index('class')))
abalone_data = abalone_data.reindex(columns=field_names)

# shuffle data
abalone_data = abalone_data.sample(frac=1)
abalone_data = abalone_data.convert_dtypes()

# normalize floating point values so that they are between 0 and 1
abalone_data = normalize(abalone_data, 2, 8, 1)

# normalize class so it is between 0 and 3
abalone_data = normalize(abalone_data, 0, 0, 3)

# round values to the nearest integer
rounding_dict = zip(field_names[2:9], itertools.repeat(0))
for field_name, zero in rounding_dict:
    abalone_data = abalone_data.round({field_name: zero})
abalone_data = abalone_data.round({field_names[0]: 0})
# convert the columns to int since they're essentially ints now
abalone_data = abalone_data.convert_dtypes()

test_train_set, validation_set = np.array_split(abalone_data.to_numpy(), [int(len(abalone_data) * .9)])
validation_set = validation_set.tolist()

# need to bin the classes into young/old?  into young/middle-aged/old?
# also will need to bin how we do classifications.... i think
perform_id3(np.array_split(test_train_set, 5), validation_set, field_names)

##########################################################################################
# image segmentation
##########################################################################################
print('\nTraining and testing segmentation dataset.')
field_names = ['class', 'REGION-CENTROID-COL', 'REGION-CENTROID-ROW', 'REGION-PIXEL-COUNT,SHORT-LINE-DENSITY-5',
               'SHORT-LINE-DENSITY-2', 'VEDGE-MEAN', 'VEDGE-SD', 'HEDGE-MEAN', 'HEDGE-SD', 'INTENSITY-MEAN',
               'RAWRED-MEAN', 'RAWBLUE-MEAN', 'RAWGREEN-MEAN', 'EXRED-MEAN', 'EXBLUE-MEAN', 'EXGREEN-MEAN',
               'VALUE-MEAN', 'SATURATION-MEAN', 'HUE-MEAN']
segmentation_data = pd.read_csv('datasets/segmentation (2).data', names=field_names, comment=';')

# remove header row
segmentation_data = segmentation_data[1:]

segmentation_data = segmentation_data.sample(frac=1)

# normalize the attributes so that they are between 1 and 0
segmentation_data = normalize(segmentation_data, 1, 18, 1)

# round values to the nearest integer
rounding_dict = zip(field_names[1:19], itertools.repeat(0))
for field_name, zero in rounding_dict:
    segmentation_data = segmentation_data.round({field_name: zero})

# convert the columns to int since they're essentially ints now
segmentation_data = segmentation_data.convert_dtypes()

test_train_set, validation_set = np.array_split(segmentation_data.to_numpy(), [int(len(segmentation_data) * .9)])
validation_set = validation_set.tolist()

perform_id3(np.array_split(test_train_set, 5), validation_set, field_names)
