import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder


def softmax_logistic_regression(weighted_features):
    """ Calculates the softmax function for linear regression.

    Args:
        weighted_features: the weighted features, equal to the dot product of the features and weights

    Returns:
        Matrix corresponding to the probabilities calculated for each class for each feature.
    """
    weighted_features -= np.max(weighted_features)
    return np.exp(weighted_features) / np.sum(np.exp(weighted_features))


def softmax_adaline(weighted_features):
    """ Calculates the softmax function for Adaline which unlike logistic regression, does not use the sigmoid function

    Args:
        weighted_features: the weighted features, equal to the dot product of the features and weights

    Returns:
        Matrix corresponding to the probabilities calculated for each class for each feature.
    """
    weighted_features -= np.max(weighted_features)
    return weighted_features / np.sum(np.exp(weighted_features))


def one_hot_encode(class_values, number_of_classes):
    """ Perform one-hot encoding of the supplied class array.

    Args:
        class_values: an array containing the classification values
        number_of_classes: the number of unique classes

    Returns:
        One hot encoded matrix of class values
    """
    # set entire matrix to zero
    one_hot_class_values = np.zeros((len(class_values), number_of_classes))

    # one-hot encode the class array
    one_hot_class_values[np.arange(len(class_values)), class_values] = 1
    return one_hot_class_values


def calc_gradient_descent(weights, features, classes, softmax):
    """ Update the weights by performing gradient descent

    Args:
        weights: The weights we will be updating
        features: The features we are training on
        classes: The training class values
        softmax: The softmax function being used

    Returns:
        Gradient matrix which will be used to update our weights.
    """
    num_classes = len(np.unique(classes))
    num_samples, num_features = features.shape
    predicted_values = softmax(np.dot(features, weights))
    one_hot_classes = one_hot_encode(classes, num_classes)
    gradient = 1 / num_samples * np.dot(features.T, (predicted_values - one_hot_classes))
    return gradient


def train(features, classes, weights, softmax_function):
    """ Function to train the model for either logistic regression or Adaline.

    This function uses a softmax function provided as a parameter to create a training model
    for either logistic regression or Adaline. The training iterations continue for at least
    50 iterations over the entire training set. The iterations continue until the difference
    between the current iteration and 50 iterations prior is less than 0.001.

    The accuracy for each iteration is calculated and is included as an output from this function.

    Args:
        features: The features to train on
        classes: The classes to train on
        weights: Weights to update during training. (initially set to random values 0 to 1)
        softmax_function: The function to use for either logistic regress or Adaline

    Returns:
        Tuple containing the weights for the model and a list containing the training accuracies as the model was built.
    """
    accuracy = []

    training_iteration_index = 0
    # continue looping until the difference between training accuracy goes below 0.001
    while training_iteration_index < 50 or abs(
            accuracy[training_iteration_index - 50] - accuracy[training_iteration_index - 1]) > 0.001:
        gradient = calc_gradient_descent(weights, features, classes, softmax_function)

        # update the weights
        weights -= alpha * gradient

        # calculate accuracy
        correct_classifications = 0
        for training_index in range(len(classes)):
            training_classifications = softmax_function(np.dot(features[training_index], weights))
            predicted_class = np.argmax(training_classifications)
            if predicted_class == classes[training_index]:
                correct_classifications += 1
        acc = correct_classifications / len(classes)
        accuracy.append(acc)
        # output the training accuracy
        if verbose and training_iteration_index % 25 == 0:
            print(f'Training Accuracy (iter={training_iteration_index}): {acc * 100:.4f}%')
        training_iteration_index += 1
    return weights, accuracy


def train_test_plot(feature_folds, class_folds, softmax, name):
    """ Helper function to perform methods that must be repeated for each dataset.

        This includes splitting up the data for cross validation, training the model,
        creating a plot of the training accuracy, testing the classifications on the model,
        and outputting the accuracy.

    Args:
        feature_folds: The features folds
        class_folds: The class folds
        softmax: The function to be used in our classification. Sigmoid for logistic
                 regression and identity for Adaline.
        name: The name of the current dataset and model. This is uesd only for output purposes.
    """
    training_accuracy = []
    test_accuracy = []
    for fold_index in range(len(feature_folds)):
        # create the cross validation train/test sets
        test_features = feature_folds[fold_index]
        test_classes = class_folds[fold_index]
        training_features = np.concatenate(np.delete(feature_folds, fold_index, axis=0))
        training_classes = np.concatenate(np.delete(class_folds, fold_index, axis=0))

        # find the number of classes, samples, and features
        num_classes = max([len(np.unique(sublist)) for sublist in class_folds])
        num_samples, num_features = training_features.shape

        # initialize our weigths to random values
        weights = np.random.random((num_features, num_classes))

        # train the model
        if verbose:
            print(f'Training fold #{fold_index + 1}')
        weights, fold_accuracy = train(training_features, training_classes, weights, softmax)
        training_accuracy.append(fold_accuracy)

        # test model
        correct_classifications = 0
        if verbose and not fold_index:
            print(f'\nOutput of test classifications for the first fold:')
        for test_index in range(len(test_classes)):
            # find the predicted class for a given test item
            test_classifications = softmax(np.dot(test_features[test_index], weights))
            predicted_class = np.argmax(test_classifications)
            if verbose and not fold_index:
                print(f'#{test_index + 1}: Predicted class: {predicted_class} Actual Class: {test_classes[test_index]}')
            if predicted_class == test_classes[test_index]:
                correct_classifications += 1

        test_accuracy.append(correct_classifications / len(test_classes))
        if verbose and not fold_index:
            print(f'Test Classification Accuracy for First Fold: {100 * test_accuracy[0]:.2f}%')
    print(f'\nModel for {name}:\n{weights}')
    avg_test_accuracy = np.average(test_accuracy) * 100
    print(f'\nAverage Test Classification Accuracy for {name}: {avg_test_accuracy:.2f}%')

    # pad the training accuracy arrays with the last value repeated, so they are all the same size
    max_accuracy_length = max([(len(sublist)) for sublist in training_accuracy])
    for index in range(len(training_accuracy)):
        training_accuracy[index] = np.pad(training_accuracy[index],
                                          (0, max_accuracy_length - len(training_accuracy[index])), mode='maximum')
    avg_training_accuracy = np.mean(training_accuracy, axis=0) * 100

    plt.figure(figsize=(10, 7))
    plt.plot(avg_training_accuracy)
    plt.title(f'{name} Training Accuracy (alpha={alpha})', fontsize=16, weight='heavy')
    plt.ylabel('Training Classification Accuracy (%)')
    plt.xlabel('Training Epochs')
    plt.savefig(name + '.png')
    plt.show()

    return avg_test_accuracy


def preprocess_iris():
    """ Normalize features
        Separates classes and features
        Renumbers the class labels to the indices of the classes.
        Creates 5 folds for cross-validation

    Returns:
        processed feature and class folds
    """
    field_names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
    data_set = pd.read_csv('datasets/iris (2).data', names=field_names)
    # randomize input
    data_set = data_set.sample(frac=1)

    # remove the class from the dataset (it was the last column)
    features = data_set.iloc[:, 0:4].values

    # maintain a separate class array
    classes = pd.factorize(data_set['class'])[0]
    class_folds = np.array_split(classes, 5)

    # insert ones as starting bias
    features = np.hstack((np.ones((len(features), 1)), features))

    # normalize to zero mean, unit variance
    features[:, 1:] = (features[:, 1:] - np.mean(features[:, 1:], axis=0)) / np.std(features[:, 1:], axis=0)
    features2 = features.copy()
    features2[:, 1:] = (features2[:, 1:] - features2[:, 1:].max()) / features2[:, 1:].min()

    feature_folds = np.array_split(features, 5)

    # return features, classes
    return feature_folds, class_folds


def preprocess_breast_cancer():
    """ Remove id column.
        Move class column to the first column.
        Re-number class from 2/4 to 0/1.
        Assign random number from 1-10 to missing values.
        Sets all feature values from 1-5 to 0 and 6-10 to 1. Trying to encode the resulting columns was causing issues.

    Returns:
        processed feature and class folds
    """
    field_names = ['id', 'clump-thickness', 'cell-size', 'cell-shape', 'adhesion', 'single-cell-size', 'base-nuclei',
                   'bland-chomatin', 'normal-nucleoli', 'mitoses', 'class']
    data_set = pd.read_csv('datasets/breast-cancer-wisconsin.data', names=field_names, dtype='O')

    # replace question marks with NaN
    data_set = data_set.replace('?', np.nan)

    # randomize the rows of input
    data_set = data_set.sample(frac=1)

    # remove id column, we don't use it in classification
    field_names.remove('id')
    data_set.drop(columns='id', inplace=True)

    # replace missing values in the data set with a random value from 1 to 10, inclusive
    data_set[pd.isna(data_set)] = np.random.randint(1, 11, (data_set.shape[0], data_set.shape[1]))
    data_set = data_set.apply(pd.to_numeric)

    # maintain a separate class array
    classes = pd.factorize(data_set['class'])[0]
    class_folds = np.array_split(classes, 5)

    # remove the class from the dataset (it was the last column)
    features = data_set.iloc[:, :9]

    # insert ones as starting bias
    features = np.hstack((np.ones((len(features), 1)), features))

    # normalize features to zero mean, unit variance
    features[:, 1:] = (features[:, 1:] - np.mean(features[:, 1:], axis=0)) / np.std(features[:, 1:], axis=0)
    feature_folds = np.array_split(features, 5)

    return feature_folds, class_folds


def preprocess_house_votes():
    """ Moves class from last column to first.
        Inserts random value, 0 or 1, for missing values.
        Changes n/y class to 0/1.

    Returns:
        processed feature and class folds
    """
    field_names = ['party', 'handicap-infants', 'water-project-cost-sharing', 'adoption-of-the-budget-resoution',
                   'physician-fee-freeze', 'el-salvador-aid', 'religious-groups-in-schools', 'anti-satellite-test-ban',
                   'aid-to-nicaraguan-contras', 'mx-missile', 'immigration', 'synfuels-cutback', 'education-spending',
                   'superfund-right-to-sue', 'crime', 'duty-free-exports', 'export-act-south-africa']
    data_set = pd.read_csv('datasets/house-votes-84.data', names=field_names)

    # replace question marks with NaN
    data_set = data_set.replace('?', np.nan)

    # randomize the rows of input
    data_set = data_set.sample(frac=1)

    # replace missing values in the data set with 'y' or 'n' at random
    data_set[pd.isna(data_set)] = np.random.choice(['y', 'n'], (data_set.shape[0], data_set.shape[1]))

    # use LabelEncoder to convert string attribute values into numeric attribute values
    data_set = data_set.apply(LabelEncoder().fit_transform)

    # maintain a separate class array
    classes = pd.factorize(data_set['party'])[0]
    class_folds = np.array_split(classes, 5)

    # remove the class from the dataset (it was the first column)
    features = data_set.iloc[:, 1:]

    # insert ones as starting bias
    features = np.hstack((np.ones((len(features), 1)), features))

    # normalize features to zero mean, unit variance
    features[:, 1:] = (features[:, 1:] - np.mean(features[:, 1:], axis=0)) / np.std(features[:, 1:], axis=0)
    feature_folds = np.array_split(features, 5)

    return feature_folds, class_folds


def preprocess_soybeans():
    """ Moves class from last column to first column.
            One-hot encodes all columns.
            Renumbers the class labels to the indices of the classes.
            Creates multiple datasets for the 4 classifiers needed.

        Returns:
            processed feature and class folds
        """
    field_names = []
    # create "labels" for the soybean's anonymous columns
    for index in range(1, 36):
        field_names.append(str(index))
    field_names.append('class')

    data_set = pd.read_csv('datasets/soybean-small.data', names=field_names, dtype='O')

    # randomize the rows of input
    data_set = data_set.sample(frac=1)

    # move the class label from the last column, to the first
    field_names.insert(0, field_names.pop(field_names.index('class')))
    data_set = data_set.reindex(columns=field_names)

    data_set = pd.get_dummies(data_set, columns=field_names[1:], prefix=field_names[1:])

    # maintain a separate class array
    classes = pd.factorize(data_set['class'])[0]
    class_folds = np.array_split(classes, 5)

    # remove the class from the dataset (it was the first column)
    features = data_set.iloc[:, 1:]
    features = features.apply(pd.to_numeric)

    # insert ones as starting bias
    features = np.hstack((np.ones((len(features), 1)), features))

    # normalize features to zero mean, unit variance
    np.seterr(all='ignore')
    features[:, 1:] = (features[:, 1:] - np.mean(features[:, 1:], axis=0)) / np.std(features[:, 1:], axis=0)
    # convert any NaN's to zeros, that resulted from inadvertant divide by zero
    # (this happens if the std dev is 0 for a column)
    features = np.nan_to_num(features)
    feature_folds = np.array_split(features, 5)

    return feature_folds, class_folds


def preprocess_glass():
    """ Removes id column and moves class to first column.
        Renumber the class labels.
        Separates into test/train folds.
        Normalizes features values

        Returns:
            processed feature and class folds
    """
    field_names = ['id', 'RI', 'Na', 'Mg', 'Al', 'Si', 'K', 'Ca', 'Ba', 'Fe', 'class']
    data_set = pd.read_csv('datasets/glass.data', names=field_names, dtype='O')

    # randomize the rows of input
    data_set = data_set.sample(frac=1)

    # remove id column, we don't use it in classification
    field_names.remove('id')
    data_set.drop(columns='id', inplace=True)

    # move the class label from the last column, to the first
    field_names.insert(0, field_names.pop(field_names.index('class')))
    data_set = data_set.reindex(columns=field_names)

    # maintain a separate class array
    classes = pd.factorize(data_set['class'])[0]
    class_folds = np.array_split(classes, 5)

    # remove the class from the dataset (it was the first column)
    features = data_set.iloc[:, 1:]
    features = features.apply(pd.to_numeric)

    # insert ones as starting bias
    features = np.hstack((np.ones((len(features), 1)), features))

    # normalize features to zero mean, unit variance
    features[:, 1:] = (features[:, 1:] - np.mean(features[:, 1:], axis=0)) / np.std(features[:, 1:], axis=0)
    feature_folds = np.array_split(features, 5)

    return feature_folds, class_folds


verbose = False


def project4():
    verbose = True
    np.random.seed(0)

    ########################################################################
    # IRIS
    alpha = 0.1
    ########################################################################

    feature_folds, class_folds = preprocess_iris()
    print(
        '\n***************************************\nIris:\tLogistic Regression\n***************************************')
    iris_logreg_accuracy = train_test_plot(feature_folds, class_folds, softmax_logistic_regression,
                                           'Iris Logistic Regression')

    feature_folds, class_folds = preprocess_iris()
    print('\n***************************************\nIris:\tAdaline\n***************************************')
    iris_adaline_accuracy = train_test_plot(feature_folds, class_folds, softmax_adaline, 'Iris Adaline')

    ########################################################################
    # Breast Cancer
    alpha = 0.01
    ########################################################################

    feature_folds, class_folds = preprocess_breast_cancer()
    print(
        '\n***************************************\nBreast Cancer:\tLogistic Regression\n***************************************')
    cancer_logreg_accuracy = train_test_plot(feature_folds, class_folds, softmax_logistic_regression,
                                             'Breast Cancer Logistic Regression')

    feature_folds, class_folds = preprocess_breast_cancer()
    print('\n***************************************\nBreast Cancer:\tAdaline\n***************************************')
    cancer_adaline_accuracy = train_test_plot(feature_folds, class_folds, softmax_adaline, 'Breast Cancer Adaline')

    ########################################################################
    # HOUSE VOTES
    alpha = 0.01
    ########################################################################

    feature_folds, class_folds = preprocess_house_votes()
    print(
        '\n***************************************\nHouse Votes:\tLogistic Regression\n***************************************')
    housevotes_logreg_accuracy = train_test_plot(feature_folds, class_folds, softmax_adaline,
                                                 'House Votes Logistic Regression')

    feature_folds, class_folds = preprocess_house_votes()
    print('\n***************************************\nHouse Votes:\tAdaline\n***************************************')
    housevotes_adaline_accuracy = train_test_plot(feature_folds, class_folds, softmax_adaline, 'House Votes Adaline')

    ########################################################################
    # SOYBEANS
    alpha = 0.01
    ########################################################################

    feature_folds, class_folds = preprocess_soybeans()
    print(
        '\n***************************************\nSoybeans:\tLogistic Regression\n***************************************')
    soybeans_logreg_accuracy = train_test_plot(feature_folds, class_folds, softmax_logistic_regression,
                                               'Soybeans Logistic Regression')

    feature_folds, class_folds = preprocess_soybeans()
    print('\n***************************************\nSoybeans:\tAdaline\n***************************************')
    soybeans_adaline_accuracy = train_test_plot(feature_folds, class_folds, softmax_adaline, 'Soybeans Adaline')

    ########################################################################
    # GLASS
    alpha = 0.05
    ########################################################################

    feature_folds, class_folds = preprocess_glass()
    print(
        '***************************************\nGlass:\tLogistic Regression\n***************************************')
    glass_logreg_accuracy = train_test_plot(feature_folds, class_folds, softmax_logistic_regression,
                                            'Glass Logistic Regression')

    feature_folds, class_folds = preprocess_glass()
    print('***************************************\nGlass:\tAdaline\n***************************************')
    glass_adaline_accuracy = train_test_plot(feature_folds, class_folds, softmax_adaline, 'Glass Adaline')

    ########################################################################
    # The code below builds a chart to display the logistic regression and adaline
    # test classification accuracies.

    def autolabel(values):
        """Attach a text label above each bar, displaying its height.
           Borrowed from matplotlib docs online:
           https://matplotlib.org/gallery/lines_bars_and_markers/barchart.html#sphx-glr-gallery-lines-bars-and-markers-barchart-py
        Args:
            values: The label values
        """

        for value in values:
            height = value.get_height()
            ax.annotate('{}'.format(height),
                        xy=(value.get_x() + value.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')

    fig, ax = plt.subplots()
    width = 0.35
    labels = ['Iris', 'Breast Cancer', 'House Votes', 'Soybeans', 'Glass']
    ind = np.arange(len(labels))
    logreg_vals = np.round((iris_logreg_accuracy, cancer_logreg_accuracy, housevotes_logreg_accuracy,
                            soybeans_logreg_accuracy, glass_logreg_accuracy), 1)
    adaline_vals = np.round((iris_adaline_accuracy, cancer_adaline_accuracy, housevotes_adaline_accuracy,
                             soybeans_adaline_accuracy, glass_adaline_accuracy), 1)
    logreg = ax.bar(ind - width / 2, logreg_vals, width, label='Logistic Regression')
    adaline = ax.bar(ind + width / 2, adaline_vals, width, label='Adaline')
    ax.set_ylabel('Test Classification Accuracy (%)')
    ax.set_title('Logistic Regression and Adaline Accuracy')
    ax.set_xticks(ind)
    ax.set_xticklabels(labels)
    ax.legend(loc='lower right')
    autolabel(logreg)
    autolabel(adaline)
    fig.tight_layout()
    plt.savefig('Project4-Test-Classification-Accuracy.png')
    plt.show()
