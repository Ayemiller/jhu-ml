from __future__ import division

import numpy as np
import numpy.random as npr
# from tensorflow.keras.datasets import mnist
from project4 import *


# import tensorflow.keras.datasets.mnist as mnist


def g(z):
    """ sigmoid """
    # return 1 / (1 + np.exp(-z))
    return np.exp(z) / (np.exp(z) + 1)


# %%

def g_(z):
    """ derivative sigmoid """
    return g(z) * (1 - g(z))


# %%

def T(y):
    """ one hot encoding """
    num_classes = len(np.unique(y))
    y = np.array(y, dtype=np.uint8)
    one_hot = np.zeros((len(y), K))
    one_hot[np.arange(len(y)), y] = 1
    return one_hot


# %%

def reverse(l):
    return l[::-1]


### Initializing weights

# %%

# inspired by:
# https://github.com/google/jax/blob/master/examples/mnist_classifier_fromscratch.py
def init_random_params(layer_sizes, rng=npr.RandomState(0)):
    return [rng.randn(nodes_in + 1, nodes_out) * np.sqrt(2 / (nodes_in + nodes_out))
            for nodes_in, nodes_out, in zip(layer_sizes[:-1], layer_sizes[1:])]


# %% md

### Feedforward

# %%

def add_bias(x):
    """ x.shape: batch * feature size """
    bias = np.ones((len(x), 1))
    return np.hstack((bias, x))


# %%

def forward(weights, inputs):
    x = inputs
    for w in weights:
        x = add_bias(x)
        x = x @ w
        x = g(x)

    return x


# %%


## Training


### Backprop

# %%

def backward(x, y, weights):
    # Feed forward, save activations
    x = inputs
    activations = [inputs]
    for w in weights:
        x = add_bias(x)
        x = x @ w
        activations.append(x)
        x = g(x)

    predictions = x

    # Get deltas, error terms
    final_error = (predictions - y).T
    errors = [final_error]
    # don't do final layer, we just did
    # don't compute error for input!
    for i, act in enumerate(activations[1:-1]):
        error = weights[-(i + 1)][1:, :] @ errors[i] * g_(
            act).T  # ignore the first weight because we don't adjust the bias
        errors.append(error)

    errors = reverse(errors)

    # Save the partial derrivatives
    grads = []

    for i in range(len(errors)):
        grad = (errors[i] @ add_bias(activations[i])) * (1 / len(y))
        grads.append(grad)

    return grads


# %% md

### Measuring performance

# %%

def stats(weights):
    m = len(y_test)
    preds = forward(weights, x_test)
    acc = (np.argmax(preds, axis=-1) == y_test).sum() / m * 100
    loss = np.sum(- np.log(preds[np.arange(m), y_test])) / m
    return {'acc': round(acc, 2), 'loss': round(loss, 2)}


# MNIST dataset
# (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
# x_train = x_train.reshape((-1, 28 * 28)) / 255
# x_test = x_test.reshape((-1, 28 * 28)) / 255
# type(x_train)

# arg1 = input layer
# arg 2 through x = num hidden nodes
# arg final = output layer
# weights = init_random_params([784, 500, 500, 10])
# print(x_train.shape[1])


feature_folds, class_folds = preprocess_iris()

test_fold_index = 0

x_test = feature_folds[test_fold_index]
y_test = class_folds[test_fold_index]

x_train = np.concatenate(np.delete(feature_folds, test_fold_index, axis=0))
y_train = np.concatenate(np.delete(class_folds, test_fold_index, axis=0))

num_inputs = x_train.shape[1]
num_hidden_nodes = 5
weights = init_random_params([num_inputs, num_hidden_nodes, num_hidden_nodes, 3])
# weights = init_random_params([num_inputs, 10])

# %%

lr = 0.001

for epoch in range(200):
    print('Starting epoch', epoch + 1)
    print(len(x_train))
    for i in range(len(x_train)):
        inputs = x_train[i][np.newaxis, :]
        # labels = T([y_train[i]], K=10)
        labels = T([y_train[i]])
        grads = backward(inputs, labels, weights)
        for j in range(len(weights)):
            weights[j] -= lr * grads[j].T
        if i % 5000 == 0: print(stats(weights))

# %%
