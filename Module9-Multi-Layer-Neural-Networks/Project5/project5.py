from __future__ import division

import copy
import warnings

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder


def one_hot_encode(class_values, number_of_classes):
    """ Perform one-hot encoding of the supplied class array.

    Args:
        class_values: an array containing the classification values
        number_of_classes: the number of unique classes

    Returns:
        One hot encoded matrix of class values
    """

    # set entire matrix to zero
    one_hot_class_values = np.zeros((len(class_values), number_of_classes))

    # one-hot encode the class array
    one_hot_class_values[np.arange(len(class_values)), class_values] = 1
    one_hot_class_values = np.array(one_hot_class_values, dtype=np.uint8)
    return one_hot_class_values


def normalize(features):
    """ Standardize w/ zero mean unit variance and normalize between zero and one

    Args:
        features: Features to normalize
    """
    # standardize to zero mean, unit variance
    features = (features - np.mean(features, axis=0)) / np.std(features, axis=0)
    # normalize to between zero and one
    features = (features - features.min()) / (features.max() - features.min())
    return features


def initialize_weight_matrix(layer_list):
    """ Helper method to create a matrix to store the weights for the hidden layer(s) and output layer.

    Returns:
        weight matrix for the layers
    """
    weight_matrix = []
    rand = np.random.RandomState(0)
    for nodes_in, nodes_out, in zip(layer_list[:-1], layer_list[1:]):
        layer = rand.randn(nodes_in + 1, nodes_out) * np.sqrt(2 / (nodes_in + nodes_out))
        weight_matrix.append(layer)
    return weight_matrix


def insert_bias_node(nodes):
    """ Helper function to insert a bias value of one into a list of nodes

    Returns:
        object: list of nodes containing newly added bias value of one
    """
    bias_node = np.ones((len(nodes), 1))
    return np.hstack((bias_node, nodes))


def forward(weight_matrix, outputs):
    """ Perform the feed-forward calculation through our network

    Returns:
        The output nodes for our dataset
    """
    for layer_weights in weight_matrix:
        outputs = np.dot(insert_bias_node(outputs), layer_weights)
        outputs = activation_function(outputs)
    return outputs


def preprocess_iris():
    """ Normalize features
        Separates classes and features
        Renumbers the class labels to the indices of the classes.
        Creates 5 folds for cross-validation

    Returns:
        processed feature and class folds
    """
    field_names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
    data_set = pd.read_csv('datasets/iris (2).data', names=field_names)
    # randomize input
    data_set = data_set.sample(frac=1)

    # remove the class from the dataset (it was the last column)
    features = data_set.iloc[:, 0:4].values

    # maintain a separate class array
    classes = pd.factorize(data_set['class'])[0]
    class_folds = np.array_split(classes, 5)
    class_folds = np.array(class_folds, dtype=np.uint8)

    features = normalize(features)
    feature_folds = np.array_split(features, 5)

    # return features, classes
    return feature_folds, class_folds


def preprocess_breast_cancer():
    """ Remove id column.
        Move class column to the first column.
        Re-number class from 2/4 to 0/1.
        Assign random number from 1-10 to missing values.
        Sets all feature values from 1-5 to 0 and 6-10 to 1. Trying to encode the resulting columns was causing issues.

    Returns:
        processed feature and class folds
    """
    field_names = ['id', 'clump-thickness', 'cell-size', 'cell-shape', 'adhesion', 'single-cell-size', 'base-nuclei',
                   'bland-chomatin', 'normal-nucleoli', 'mitoses', 'class']
    data_set = pd.read_csv('datasets/breast-cancer-wisconsin.data', names=field_names, dtype='O')

    # replace question marks with NaN
    data_set = data_set.replace('?', np.nan)

    # randomize the rows of input
    data_set = data_set.sample(frac=1)

    # remove id column, we don't use it in classification
    field_names.remove('id')
    data_set.drop(columns='id', inplace=True)

    # replace missing values in the data set with a random value from 1 to 10, inclusive
    data_set[pd.isna(data_set)] = np.random.randint(1, 11, (data_set.shape[0], data_set.shape[1]))
    data_set = data_set.apply(pd.to_numeric)

    # maintain a separate class array
    classes = pd.factorize(data_set['class'])[0]
    class_folds = np.array_split(classes, 5)

    # remove the class from the dataset (it was the last column)
    features = data_set.iloc[:, :9]

    # normalize features to zero mean, unit variance
    features = normalize(features.to_numpy())
    feature_folds = np.array_split(features, 5)

    return feature_folds, class_folds


def preprocess_house_votes():
    """ Moves class from last column to first.
        Inserts random value, 0 or 1, for missing values.
        Changes n/y class to 0/1.

    Returns:
        processed feature and class folds
    """
    field_names = ['party', 'handicap-infants', 'water-project-cost-sharing', 'adoption-of-the-budget-resoution',
                   'physician-fee-freeze', 'el-salvador-aid', 'religious-groups-in-schools', 'anti-satellite-test-ban',
                   'aid-to-nicaraguan-contras', 'mx-missile', 'immigration', 'synfuels-cutback', 'education-spending',
                   'superfund-right-to-sue', 'crime', 'duty-free-exports', 'export-act-south-africa']
    data_set = pd.read_csv('datasets/house-votes-84.data', names=field_names)

    # replace question marks with NaN
    data_set = data_set.replace('?', np.nan)

    # randomize the rows of input
    data_set = data_set.sample(frac=1)

    # replace missing values in the data set with 'y' or 'n' at random
    data_set[pd.isna(data_set)] = np.random.choice(['y', 'n'], (data_set.shape[0], data_set.shape[1]))

    # use LabelEncoder to convert string attribute values into numeric attribute values
    data_set = data_set.apply(LabelEncoder().fit_transform)

    # maintain a separate class array
    classes = pd.factorize(data_set['party'])[0]
    class_folds = np.array_split(classes, 5)

    # remove the class from the dataset (it was the first column)
    features = data_set.iloc[:, 1:]

    # normalize features to zero mean, unit variance
    features = normalize(features.to_numpy())
    feature_folds = np.array_split(features, 5)

    return feature_folds, class_folds


def preprocess_soybeans():
    """ Moves class from last column to first column.
            One-hot encodes all columns.
            Renumbers the class labels to the indices of the classes.
            Creates multiple datasets for the 4 classifiers needed.

        Returns:
            processed feature and class folds
        """
    field_names = []
    # create "labels" for the soybean's anonymous columns
    for index in range(1, 36):
        field_names.append(str(index))
    field_names.append('class')

    data_set = pd.read_csv('datasets/soybean-small.data', names=field_names, dtype='O')

    # randomize the rows of input
    data_set = data_set.sample(frac=1)

    # move the class label from the last column, to the first
    field_names.insert(0, field_names.pop(field_names.index('class')))
    data_set = data_set.reindex(columns=field_names)

    data_set = pd.get_dummies(data_set, columns=field_names[1:], prefix=field_names[1:])

    # maintain a separate class array
    classes = pd.factorize(data_set['class'])[0]
    class_folds = np.array_split(classes, 5)

    # remove the class from the dataset (it was the first column)
    features = data_set.iloc[:, 1:]
    features = features.apply(pd.to_numeric)

    # normalize features to zero mean, unit variance
    np.seterr(all='ignore')
    features = normalize(features)
    # convert any NaN's to zeros, that resulted from inadvertant divide by zero
    # (this happens if the std dev is 0 for a column)
    features = np.nan_to_num(features)
    feature_folds = np.array_split(features, 5)

    return feature_folds, class_folds


def preprocess_glass():
    """ Removes id column and moves class to first column.
        Renumber the class labels.
        Separates into test/train folds.
        Normalizes features values

        Returns:
            processed feature and class folds
    """
    field_names = ['id', 'RI', 'Na', 'Mg', 'Al', 'Si', 'K', 'Ca', 'Ba', 'Fe', 'class']
    data_set = pd.read_csv('datasets/glass.data', names=field_names, dtype='O')

    # randomize the rows of input
    data_set = data_set.sample(frac=1)

    # remove id column, we don't use it in classification
    field_names.remove('id')
    data_set.drop(columns='id', inplace=True)

    # move the class label from the last column, to the first
    field_names.insert(0, field_names.pop(field_names.index('class')))
    data_set = data_set.reindex(columns=field_names)

    # remove classes container, tableware, and vehicle windows (float-processed) since they are under represented
    data_set = data_set[data_set['class'] != '3']
    data_set = data_set[data_set['class'] != '5']
    data_set = data_set[data_set['class'] != '6']

    # maintain a separate class array
    classes = pd.factorize(data_set['class'])[0]
    class_folds = np.array_split(classes, 5)

    # remove the class from the dataset (it was the first column)
    features = data_set.iloc[:, 1:]
    features = features.apply(pd.to_numeric)

    # normalize features to zero mean, unit variance
    features = normalize(features.to_numpy())
    feature_folds = np.array_split(features, 5)

    return feature_folds, class_folds


def backprop(features, label, weights):
    """ Perform backprop algorithm. The algorithm initially performs a feed-forward step
        inorder to find the errors in the outputs. Next we calculate the errors in the hidden
        layers (if any). Finally, we iterate through the errors that we calculated, and calculate
        the dot product of our errors and outputs in order to get our error gradients.


    Args:
        features:
        label:
        weights:

    Returns:
        error gradients
    """
    # perform the feed-forward step
    outputs = features
    layer_outputs = [features]
    for weight in weights:
        outputs = np.dot(insert_bias_node(outputs), weight)
        layer_outputs.append(outputs)
        outputs = activation_function(outputs)

    # subtract class labels from outputs to get errors
    errors = [(outputs - label).transpose()]

    # add the errors from the hidden layers
    for activation_index, activation in enumerate(layer_outputs[1:-1]):
        error = np.dot(weights[-(activation_index + 1)][1:, :], errors[activation_index]) * activation_derivative(
            activation).transpose()
        errors.append(error)

    # reverse the errors so that we can directly work with the errors and activation outputs
    errors = errors[::-1]

    error_gradients = []
    for error_index in range(len(errors)):
        error_gradients.append(
            np.dot(errors[error_index], insert_bias_node(layer_outputs[error_index])) * (1 / len(label)))

    return error_gradients


def activation_function(input_value):
    """ Activation function for neural network.
        Both the sigmoid and tanh are included, but only one can be enabled at once.

    Args:
        z:

    Returns:
        the output of the activation function, either sigmoid or tanh
    """
    # Sigmoid
    # return 1 / (1 + np.exp(-input_value))

    # TanH
    return (np.exp(input_value) - np.exp(-input_value)) / (np.exp(input_value) + np.exp(-input_value))


def activation_derivative(input_value):
    """ Activation function for neural network.
        Both the sigmoid and tanh derivatives are included, but only one can be enabled at once.

    Returns:
        the output of the activation function's derivative, either sigmoid' or tanh'
    """
    # Sigmoid Derivative
    # return activation(input_value) * (1 - activation(input_value))

    # TanH Derivative
    return 4 / np.power((np.exp(input_value) + np.exp(-input_value)), 2)


def print_weights(weights):
    """ Helper function to print out a numpy array in a legible form

    Args:
        weights:
    """
    dataframe = pd.DataFrame(weights)
    dataframe.columns = [''] * dataframe.shape[1]
    print(dataframe.to_string(index=False, float_format="{:,.3f}".format))
    print()


def train(training_features, training_classes, weights, min_number_of_epochs):
    """ Function to train the model for back propagation with a variable number of hidden layers and nodes.

    Args:
        training_features: The features to train on
        training_classes: The classes to train on
        weights: The weights to update during training
        min_number_of_epochs: The minimum number of epochs during training

    Returns:

    """
    fold_accuracy = []
    epoch_index = 0
    num_classes = len(np.unique(training_classes))

    # continue training until the accuracy begins to decline
    while epoch_index < min_number_of_epochs or fold_accuracy[epoch_index - 1] > fold_accuracy[epoch_index - 2]:

        # iterate through all of our training items
        for training_index in range(len(training_features)):
            item_features = training_features[training_index][np.newaxis, :]
            item_class = one_hot_encode([training_classes[training_index]], num_classes)

            # perform backprop to get our error gradients
            gradients = backprop(item_features, item_class, weights)

            # update the weights
            for weight_index in range(len(weights)):
                # print_weights(weights[weight_index])
                weights[weight_index] -= learning_rate * gradients[weight_index].transpose()
                # print_weights(weights[weight_index])

        # calculate training accuracy
        class_probabilities = forward(weights, training_features)
        training_accuracy = round(
            (np.argmax(class_probabilities, axis=-1) == training_classes).sum() / len(training_classes) * 100, 2)
        fold_accuracy.append(training_accuracy)
        epoch_index += 1
    fold_accuracy = fold_accuracy[:-1]
    return weights, fold_accuracy


def train_test_plot(feature_folds, class_folds, name, learning_rate, min_number_of_epochs, num_hidden_layers,
                    num_hidden_nodes):
    """ Helper function to perform methods that must be repeated for each dataset.

        This includes splitting up the data for cross validation, training the model,
        creating a plot of the training accuracy, testing the classifications on the model,
        and outputting the accuracy.

    Args:
        feature_folds: The feature folds
        class_folds: The class folds
        name: The "name" of the algorithm and data set being used
        learning_rate: The learning rate for backpropagation
        min_number_of_epochs: The min number of epochs that we should use for training
        num_hidden_layers: The number of hidden layers for backpropagation
        num_hidden_nodes: The number of nodes for each hidden layer

    Returns:
        The average test accuracy
    """
    print(f'\n***************************************\n{title}\n***************************************')
    test_accuracy = []
    training_accuracy = []
    for fold_index in range(len(feature_folds)):

        test_features = feature_folds[fold_index]
        test_classes = class_folds[fold_index]

        training_features = np.concatenate(np.delete(feature_folds, fold_index, axis=0))
        training_classes = np.concatenate(np.delete(class_folds, fold_index, axis=0))

        num_inputs = training_features.shape[1]
        num_classes = len(np.unique(training_classes))
        nodes_per_layer = [num_inputs]
        for _ in range(num_hidden_layers):
            nodes_per_layer.append(num_hidden_nodes)
        nodes_per_layer.append(num_classes)
        weights = initialize_weight_matrix(nodes_per_layer)
        # train the model
        weights, fold_accuracy = train(training_features, training_classes, weights, min_number_of_epochs)

        # calculate test accuracy
        if verbose and not fold_index:
            print(f'\nOutput of test classifications for the first fold:')
        class_probabilities = forward(weights, test_features)
        correct_classifications = 0
        for test_index in range(len(class_probabilities)):
            predicted_class = np.argmax(class_probabilities[test_index])
            if verbose and not fold_index:
                print(f'#{test_index + 1}: Predicted class: {predicted_class} Actual Class: {test_classes[test_index]}')
            if predicted_class == test_classes[test_index]:
                correct_classifications += 1
        test_accuracy.append(correct_classifications / len(test_classes) * 100)
        training_accuracy.append(fold_accuracy)

    # pad the training accuracy arrays with the last value repeated, so they are all the same size
    max_accuracy_length = max([(len(sublist)) for sublist in training_accuracy])
    for index in range(len(training_accuracy)):
        training_accuracy[index] = np.pad(training_accuracy[index],
                                          (0, max_accuracy_length - len(training_accuracy[index])), mode='maximum')
    avg_training_accuracy = np.mean(training_accuracy, axis=0)

    avg_test_accuracy = np.average(test_accuracy)
    print(f'\nAverage Test Classification Accuracy for {name}: {avg_test_accuracy:.2f}%')
    plt.figure(figsize=(10, 7))
    for training_acc in training_accuracy:
        plt.plot(training_acc, linestyle=':')
    plt.plot(avg_training_accuracy)
    plt.title(f'{name} Avg Accuracy During Training (alpha={learning_rate})', fontsize=16, weight='heavy')
    plt.ylabel('Test Classification Accuracy (%)')
    plt.xlabel('Training Epochs')
    plt.savefig(name + '.png')
    plt.show()
    return avg_test_accuracy


np.seterr(all='ignore')
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)
iris_accuracy = []
cancer_accuracy = []
votes_accuracy = []
soybean_accuracy = []
glass_accuracy = []
verbose = True
# Iris
feature_folds, class_folds = preprocess_iris()
title = "Iris - 2 Hidden Layers"
learning_rate = 0.03
iris_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 50, 2, 10))

title = "Iris - 1 Hidden Layer"
learning_rate = 0.01
iris_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 30, 1, 10))
title = "Iris - 0 Hidden Layers"
learning_rate = 0.01
iris_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 75, 0, 0))
iris_accuracy = iris_accuracy[::-1]

# Breast Cancer
feature_folds, class_folds = preprocess_breast_cancer()
title = "Cancer - 2 Hidden Layers"
learning_rate = 0.005
cancer_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 4, 2, 10))
title = "Cancer - 1 Hidden Layer"
learning_rate = 0.01
cancer_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 20, 1, 10))

learning_rate = 0.01
title = "Cancer - 0 Hidden Layers"
cancer_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 30, 0, 0))
cancer_accuracy = cancer_accuracy[::-1]

# House Votes
feature_folds, class_folds = preprocess_house_votes()
learning_rate = 0.05
title = "Votes - 2 Hidden Layers"

votes_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 6, 2, 10))

learning_rate = 0.01
title = "Votes - 1 Hidden Layer"
votes_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 20, 1, 10))

learning_rate = 0.01
title = "Votes - 0 Hidden Layers"
votes_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 30, 0, 0))
votes_accuracy = votes_accuracy[::-1]

# Soybeans
feature_folds, class_folds = preprocess_soybeans()
learning_rate = 0.05
title = "Soybean - 2 Hidden Layers"
soybean_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 4, 2, 10))

learning_rate = 0.05
title = "Soybean - 1 Hidden Layer"
soybean_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 4, 1, 10))

learning_rate = 0.01
title = "Soybean - 0 Hidden Layers"
soybean_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 6, 0, 0))
soybean_accuracy = soybean_accuracy[::-1]

# Glass
feature_folds, class_folds = preprocess_glass()
learning_rate = 0.02
title = "Glass - 2 Hidden Layers"
glass_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 50, 2, 80))

learning_rate = 0.01
title = "Glass - 1 Hidden Layer"
glass_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 60, 1, 50))

learning_rate = 0.05
title = "Glass - 0 Hidden Layers"
glass_accuracy.append(
    train_test_plot(copy.deepcopy(feature_folds), copy.deepcopy(class_folds), title, learning_rate, 20, 0, 0))
glass_accuracy = glass_accuracy[::-1]


########################################################################
# The code below builds a chart to display the backpropagation test classification accuracies.

def autolabel(values):
    """Attach a text label above each bar, displaying its height.
       Borrowed from matplotlib docs online:
       https://matplotlib.org/gallery/lines_bars_and_markers/barchart.html#sphx-glr-gallery-lines-bars-and-markers-barchart-py
    Args:
        values: The label values
    """

    for value in values:
        height = value.get_height()
        ax.annotate('{}'.format(height),
                    xy=(value.get_x() + value.get_width() / 3, height),
                    xytext=(1, 3),  # offset the text
                    textcoords="offset points",
                    ha='center', va='bottom')  # , fontsize=10)


plt.figure(figsize=(11, 7))
fig, ax = plt.subplots()
width = 0.3
labels = ['Iris', 'Breast Cancer', 'House Votes', 'Soybeans', 'Glass']
ind = np.arange(len(labels))
zero_hidden_vals = np.rint(
    (iris_accuracy[0], cancer_accuracy[0], votes_accuracy[0], soybean_accuracy[0], glass_accuracy[0])).astype(int)
one_hidden_vals = np.rint(
    (iris_accuracy[1], cancer_accuracy[1], votes_accuracy[1], soybean_accuracy[1], glass_accuracy[1])).astype(int)
two_hidden_vals = np.rint(
    (iris_accuracy[2], cancer_accuracy[2], votes_accuracy[2], soybean_accuracy[2], glass_accuracy[2])).astype(int)
zero_hidden = ax.bar(ind - width, zero_hidden_vals, width, label='Zero Hidden Layers')
one_hidden = ax.bar(ind, one_hidden_vals, width, label='One Hidden Layer')
two_hidden = ax.bar(ind + width, two_hidden_vals, width, label='Two Hidden Layers')
ax.set_ylabel('Test Classification Accuracy (%)')
ax.set_title('Backpropagation Accuracy')
ax.set_xticks(ind)
ax.set_xticklabels(labels)
ax.legend(loc='lower right')
autolabel(zero_hidden)
autolabel(one_hidden)
autolabel(two_hidden)
fig.tight_layout()
plt.savefig('Project5-Neural-Network-Classification-Accuracy.png')
plt.show()
