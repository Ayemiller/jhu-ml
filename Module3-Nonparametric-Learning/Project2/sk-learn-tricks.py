import pandas as pd
from sklearn.model_selection import StratifiedKFold

# STRATIFIED 5-FOLD

input_csv = pd.read_csv('datasets/ecoli.data', header=None, delim_whitespace=True)
input_csv = input_csv.drop(input_csv.columns[0], axis=1)

skf = StratifiedKFold(n_splits=5)
x = input_csv.iloc[:, :-1].to_numpy()
Y = input_csv.iloc[:, -1].to_numpy()
strat = skf.get_n_splits(x, Y)
for train_index, test_index in skf.split(x, Y):
    train_features, test_features = x[train_index], x[test_index]
    train_classes, test_classes = Y[train_index], Y[test_index]
    print("TRAIN1:", train_features, "TEST1:", test_features)
    print()
    print("TRAIN2:", train_classes, "TEST2:", test_classes)
