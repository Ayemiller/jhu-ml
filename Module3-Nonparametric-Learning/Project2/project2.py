from __future__ import division

import calendar
import random
from collections import defaultdict
from math import sqrt

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def knn(minK, maxK, folds, isRegression):
    """ Function that performs knn as described in this module's pseudocode.

    This function performs kNN on test_data by using the "model" of training_data.
    The function will output a dict that contains the mean squared error for all
    values of k where minK <= k <= maxK.

    Instead of having this function return the values of the nearest neighbors,
    I've chosen to return the mean squared error. By doing this, the same test_item
    of sorted neighbors can be saved and reused, which greatly speeds up
    kNN on the same data for different k values.

    Args:
        minK: The lower bound of k for kNN to perform.
        maxK: The upper bound of k for kNN to perform.
        test_data: The "test" data for kNN.
        training_data: The "training" data for kNN.
        isRegression: Perform regression if true, otherwise, classification.

    Returns:
        Dict containing key of the "k" value mapped to its MSE.

    """
    test_regression_error = defaultdict(lambda: 0.0)
    test_classifications = []
    total_test_instances = 0
    for test_fold_index in range(0, len(folds)):
        test_data = folds[test_fold_index]
        total_test_instances += len(test_data)
        training_data = np.concatenate(np.delete(folds, test_fold_index, axis=0))
        for test_item in test_data:
            distances = get_distances(test_item, training_data)
            # sort primarily by the distances, and if there are ties, sort by smallest class label
            sorted_vals = sorted(distances, key=lambda x: (x[0], x[1]))
            test_classification = {}
            for k in range(minK, maxK + 1):
                if isRegression:
                    k_nearest = [float(tup[1]) for tup in sorted_vals[:k]]
                    # add the mean squared error for this k-value
                    test_regression_error[k] += (np.mean(k_nearest) - float(test_item[len(test_item) - 1])) ** 2
                else:
                    k_nearest = [tup[1] for tup in sorted_vals[:k]]
                    class_count = defaultdict(lambda: 0)
                    for classification in k_nearest:
                        class_count[classification] += 1
                    closest_with_count = sorted(class_count.items(), key=lambda x: x[1], reverse=True)
                    test_classification[k] = closest_with_count[0][0]
            test_classifications.append((test_item[len(test_item) - 1], test_classification))
    if isRegression:
        # multiply by "1/n" for MSE
        for k in range(minK, maxK + 1):
            test_regression_error[k] /= total_test_instances
        return test_regression_error
    else:
        return test_classifications


def get_distances(test_item, training_data):
    distances = []
    for training_item in training_data:
        distance = sum(
            [((float(training_item[i]) - float(test_item[i])) ** 2) for i in range(0, len(test_item) - 1)])
        # append tuple containing distance and class label
        distances.append((sqrt(distance), training_item[len(training_item) - 1]))
    return distances


def edited_knn(minK, maxK, test_items, training_data):
    test_classifications = []
    for test_item in test_items:
        distances = get_distances(test_item, training_data)

        # sort primarily by the distances, and if there are ties, sort by smallest class label
        sorted_vals = sorted(distances, key=lambda x: (x[0], x[1]))

        # don't allow exact matches 
        for item in sorted_vals:
            if item[0] == 0:
                sorted_vals = sorted_vals[1:]

        test_classification = {}
        for k in range(minK, maxK + 1):
            k_nearest = [tup[1] for tup in sorted_vals[:k]]
            class_count = defaultdict(lambda: 0)
            for classification in k_nearest:
                class_count[classification] += 1
            closest_with_count = sorted(class_count.items(), key=lambda x: x[1], reverse=True)
            test_classification[k] = closest_with_count[0][0]
        test_classifications.append((test_item[len(test_item) - 1], test_classification))
    return test_classifications


def get_stratified_k_fold_sets(array, k):
    """ Helper function to split up dataset into a stratified k-fold dataset

    Args:
        array: The input data set with class labels in the last column.

    Returns:
        List containing k stratified folds for provided dataset.

    """
    folds = [[] for x in range(k)]

    # get the different unique class labels
    for unique_class in np.unique(array[:, -1]):
        # get all the entries corresponding to this class
        rows_in_class = array[np.where(array[:, -1] == unique_class)]

        # Create a list of indices that is a repeated sampling of 1 through k for the length of
        # rows_in_class. This will provide a stratified split of classes in our folds.
        fold_indices = []
        for _ in range(0, int(len(rows_in_class) / k) + 1):
            fold_indices.extend([i for i in random.sample(range(0, k), k)])
        fold_indices = fold_indices[:len(rows_in_class)]

        # Insert each row into one of the folds, specified by the fold_inices list.
        for index, row in enumerate(rows_in_class):
            folds[fold_indices[index]].append(row)

    # mix up the folds
    [np.random.shuffle(fold) for fold in folds]

    return folds


def print_classification_metrics(classification_results, name):
    correct, errors = calc_errors(classification_results)
    print(f'\n{name.upper()}\nTotal Number of Classifications: {len(classification_results)}')
    classification_error = {}

    for k in classification_results[0][1].keys():
        print(f'k={k}')
        print(f'Errors: {errors[k]}')
        print(f'Correct k={k}: {correct[k]}')
        classification_error[k] = errors[k] / len(classification_results) * 100
        print(f'Classification Error: {classification_error[k]:.2f}%')
        print()
    plot_error(classification_error, name, 'Classification Error (%)')
    return classification_error


def print_regression_metrics(regression_results, name, num_samples):
    print(f'\n{name.upper()}\nTotal Number of Samples: {num_samples}')
    for k in regression_results.keys():
        print(f'MSE for k={k}: {regression_results[k]}')
    plot_error(regression_results, name, 'Mean Squared Error')


def calc_errors(classification_results):
    errors = defaultdict(lambda: 0)
    correct = defaultdict(lambda: 0)
    for actual_test_class, test_classification in classification_results:
        for k in range(1, len(test_classification) + 1):
            if actual_test_class != test_classification[k]:
                errors[k] += 1
            else:
                correct[k] += 1
    return correct, errors


def plot_error(error_dict, plot_name, y_label):
    plt.figure(figsize=(10, 7))
    plt.plot(list(error_dict.keys()), list(error_dict.values()), color='b', label='Test Error')
    plt.xlabel('k Nearest Neighbors')
    plt.ylabel(y_label)
    plt.title(plot_name + ' Classification Error', fontsize=16, weight='heavy')
    plt.savefig(plot_name + '.png')
    plt.show()
    print()


def classify(array, name):
    classification_errors = []
    for _ in range(0, 5):
        folds = get_stratified_k_fold_sets(array, 5)
        classification_errors.extend(knn(1, 10, folds, False))
    classification_errors = print_classification_metrics(classification_errors, name)
    return classification_errors


def edit_classify(edited_training_array, array, name):
    classification_errors = []
    folds = get_stratified_k_fold_sets(array, 5)
    for fold in folds:
        np.random.shuffle(edited_training_array)
        classification_errors.extend(edited_knn(1, 10, fold, edited_training_array))
    return classification_errors


def edited_knn_classify(array, name):
    classification_errors = []
    # perform our edits to the dataset here
    for _ in range(0, 3):
        edited_training_array = array.copy()
        i = 0
        while i != len(edited_training_array):
            test = edited_training_array[i]
            train = np.delete(edited_training_array, i, axis=0)
            edited_errors = edited_knn(1, 10, [test], train)
            # "grade" the error. If there are errors, remove test from the data set. Proceed to next.
            correct, errors = calc_errors(edited_errors)
            if sum(errors.values()) > 0:
                edited_training_array = np.delete(edited_training_array, i, 0)
            else:
                i += 1

        # now classify on our edited training set
        classification_errors.extend(edit_classify(edited_training_array, array, name))
    return print_classification_metrics(classification_errors, name)


def condensed_knn_classify(array, name):
    classification_errors = []

    for _ in range(0, 1):
        temp_array = array.copy()
        condensed_training_array = []

        # add initial point
        random_row_index = np.random.choice(array.shape[0])
        random_row = array[random_row_index, :]
        condensed_training_array.append(random_row)

        old_length = -1
        while (len(condensed_training_array) != old_length):
            old_length = len(condensed_training_array)
            np.random.shuffle(array)
            for row in array:
                # find closest point to row
                distances = get_distances(row, condensed_training_array)
                sorted_vals = sorted(distances, key=lambda x: (x[0], x[1]))
                closest_item = sorted_vals[0]

                # if the labels don't match, add it to the condensed set
                if closest_item[1] != row[len(row) - 1]:
                    condensed_training_array.append(row)
        condensed_training_array = np.array(condensed_training_array)

        # now classify on our edited training set
        classification_errors.extend(edit_classify(condensed_training_array, array, name))
    return print_classification_metrics(classification_errors, name)


def normalize(dataframe):
    # shuffle the rows
    dataframe = dataframe.sample(frac=1)

    for index in range(len(dataframe.columns) - 1):
        column = dataframe.columns[index]
        dataframe[column] = dataframe[column].apply(pd.to_numeric)
        max_value = dataframe[column].max()
        min_value = dataframe[column].min()

        # perform min-max normalization
        dataframe[column] = (dataframe[column] - min_value) / ((max_value - min_value) + 0.00000001)
    return dataframe


# ************************************************************************************
# ECOLI (classification)
# ************************************************************************************

ecoli_csv = pd.read_csv('datasets/ecoli.data', header=None, delim_whitespace=True)

# remove the unused name column
ecoli_csv = ecoli_csv.drop(ecoli_csv.columns[0], axis=1)

# remove the three classes with few samples: imS, imL, omL
ecoli_csv = ecoli_csv[~ecoli_csv[8].isin(['imS', 'imL', 'omL'])]

# normalize features
ecoli_csv = normalize(ecoli_csv)

# perform knn classification
ecoli_knn_error = classify(ecoli_csv.to_numpy(), 'Ecoli KNN Classification')

# perform edited knn classification
ecoli_edit_error = edited_knn_classify(ecoli_csv.to_numpy(), 'Ecoli Edited KNN Classification')

# perform condensed knn classification
ecoli_condensed_error = condensed_knn_classify(ecoli_csv.to_numpy(), 'Ecoli Condensed KNN Classification')

# plot of all of the KNN algos for ecoli
plt.figure(figsize=(10, 7))
plt.plot(list(ecoli_knn_error.keys()), list(ecoli_knn_error.values()), color='r', label='KNN')
plt.plot(list(ecoli_edit_error.keys()), list(ecoli_edit_error.values()), color='b', label='Edited KNN')
plt.plot(list(ecoli_condensed_error.keys()), list(ecoli_condensed_error.values()), color='g', label='Condensed KNN')
plt.legend()
plt.xlabel('k Nearest Neighbors')
plt.ylabel('Classification Error (%)')
plt.title('Ecoli KNN Classification Error Comparison', fontsize=16, weight='heavy')
plt.savefig('Ecoli KNN Classification Error Comparison' + '.png')
plt.show()
print()
# ************************************************************************************
# SEGMENTATION (classification)
# ************************************************************************************
seg_csv = pd.read_csv('datasets/segmentation.data', names=[str(x) for x in range(20)], comment=';')

# remove header row
seg_csv = seg_csv[1:]

# move class to last column
seg_csv['20'] = seg_csv.pop('0')

# normalize features
seg_csv = normalize(seg_csv)

# perform knn classification
image_error = classify(seg_csv.to_numpy(), 'Image Segmentation KNN Classification')

# perform edited knn classification
image_edited_error = edited_knn_classify(seg_csv.to_numpy(), 'Image Segmentation Edited KNN Classification')

# perform condensed knn classification
image_condensed_error = condensed_knn_classify(seg_csv.to_numpy(), 'Image Segmentation Condensed KNN Classification')

# plot of all of the KNN algos for segementation
plt.figure(figsize=(10, 7))
plt.plot(list(image_error.keys()), list(image_error.values()), color='r', label='KNN')
plt.plot(list(image_edited_error.keys()), list(image_edited_error.values()), color='b', label='Edited KNN')
plt.plot(list(image_condensed_error.keys()), list(image_condensed_error.values()), color='g', label='Condensed KNN')
plt.legend()
plt.xlabel('k Nearest Neighbors')
plt.ylabel('Classification Error (%)')
plt.title('Segmentation KNN Classification Error Comparison', fontsize=16, weight='heavy')
plt.savefig('Segmentation KNN Classification Error Comparison' + '.png')
plt.show()
print()
# ************************************************************************************
# MACHINE (regression)
# ************************************************************************************
machine_csv = pd.read_csv('datasets/machine.data', header=None)

# delete columns 0 and 1
machine_csv.drop(machine_csv.columns[[0, 1]], axis=1, inplace=True)

# delete the "ERP", or provided regression column, but we'll keep it for later
provided_regression_prediction = machine_csv.pop(9).to_numpy()

# attempt to normalize the second and third columns so that they are not so dominant in the calculation
machine_csv = normalize(machine_csv)

folds = np.array_split(machine_csv.to_numpy(), 5)
[np.random.shuffle(fold) for fold in folds]

machine_mse = knn(1, 10, folds, True)
print_regression_metrics(machine_mse, 'CPU Performance KNN Regression', len(machine_csv))

# ************************************************************************************
# FOREST FIRES (regression)
# ************************************************************************************
forestfires_csv = pd.read_csv('datasets/forestfires.csv')

# transform months from month to numeric
forestfires_csv['month'] = forestfires_csv['month'].apply(
    lambda x: [month.lower() for month in list(calendar.month_abbr)][1:].index(x) + 1)

# transform days from mon-to-sun to numeric
forestfires_csv['day'] = forestfires_csv['day'].apply(
    lambda x: [day.lower() for day in list(calendar.day_abbr)].index(x) + 1)

forestfires_csv = normalize(forestfires_csv)
folds = np.array_split(forestfires_csv.to_numpy(), 5)
[np.random.shuffle(fold) for fold in folds]

forestfires_mse = knn(1, 10, folds, True)
print_regression_metrics(forestfires_mse, 'Forest Fire KNN Regression', len(forestfires_csv))

# plot of all of the KNN algos for segementation
plt.figure(figsize=(10, 7))
plt.plot(list(machine_mse.keys()), list(machine_mse.values()), color='r', label='CPU Performance')
plt.plot(list(forestfires_mse.keys()), list(forestfires_mse.values()), color='b', label='Forest Fire')
plt.legend()
plt.xlabel('k Nearest Neighbors')
plt.ylabel('MSE')
plt.title('CPU and Forest Fire MSE Comparison', fontsize=16, weight='heavy')
plt.savefig('CPU and Forest Fire Error Comparison' + '.png')
plt.show()
print()
# ************************************************************************************
